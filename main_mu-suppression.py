# This file is going to access all the functions used to calculate 
# Mu suppression values 
# sample rate = 250Hz = 0.004s per sample 
# comput the MSI per trial and then average

#%%
''' SEGMENT INTO (MI AND REST) '''
from fileinput import filename
#from PSD_plots import PSD_per_run
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_saved_trials = multiple_subects_pd(path_file_trials())

#%%
# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = inria_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(inria_saved_trials[run])
    run_dict[run] = loaded_tials

#%%
# segment all trials into MI and rest 
key_runs = run_dict.keys()
run_MI_rest = {}
for run in key_runs:
    from segmentation import crop_MI_rest
    trial_MI, trial_rest = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]

#%%
from negative_MSI import PSD_run
PSD_per_run = PSD_run(run_MI_rest)

#%%
#print(PSD_per_run.keys())
for _ in PSD_per_run.keys():
    print(PSD_per_run[_])
#%%
print(PSD_per_run.keys())
#%%
"""
FOR EACH TRIAL CALCULATE THE MU-SUPPRESSION INDEX OR MSI
IT IS NECESSARY TO HAVE THE FOLLOWING VALUES:
    DATA SEGEMENTED INTO:
        RIGHT VS LEFT 
        MI VS REST
        EACH CHANNEL 
    (THIS IS ALREDAY DONE IF i LOADE THE DATA AS ABOVE)

    FOR EACH SUBJECT 
        GET ALL TRIALS
        FOR EACH TRIAL 
            DEVIDE INTO RIGHT OR LEFT
                COMPUTE PSD
                FOR MI C3, MI C4, REST C3, REST C4
                    AVERAGE PSD TO GET ONE VALUE 
    RETURN A DF 
        ROW = SUBJECT, RUN, TRIAL, LEFT/RIGHT, (4X DIVISIONS), MSI
"""

#%% 
from mu_suppression import get_PSD_values
df = get_PSD_values(run_MI_rest=run_MI_rest)
df
#%%
from negative_MSI import plots_negative_trials
fig_list = plots_negative_trials(df, PSD_per_run)
#%%
print(len(fig_list))
#%%
neg = df[df["MSI"] < 0]
neg
#%%
from mu_suppression import MSI_per_run
df_run = MSI_per_run(df)
df_run
#%%
from mu_suppression import MSI_per_subject
df_avg = MSI_per_subject(df)
df_avg

#%%
'''
# Save the DF into CSV files in order to access them later 
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\PSD.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_run.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_run.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_avg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_avg.to_csv(filepath)

# Save the DF as an image, for easy readability
# You can format the table
from numpy import genfromtxt
from matplotlib import pyplot
from matplotlib.image import imread, imsave
my_data = genfromtxt(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_avg.csv", delimiter=',')
imsave(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_avg.png", my_data) #, cmap='gray')
image_1 = imread('path to read image as Ex: output.png')
# plot raw pixel data
pyplot.imshow(image_1)
# show the figure
pyplot.show()
'''
