
import mne
import os

def right_left_tiral(data, trials_left, trials_right):
    '''
    Input:  Dictionary with the name of the individual
            trial number and the data about that trial
    Function:   Add if it is a right or left trial to the 
                existing trial before it is safed 
    Output: Dictionary distinguishing trial numbers into 
            right or left
    '''
    df_data = data.keys()
    trial_type = {}
    for file_t in df_data:
        if file_t in trials_left.keys():
            #str_save = str(file_t) + "_left"
            trial_type[(str(file_t)+"_left")] = data[file_t]
        elif file_t in trials_right.keys():
            #str_save = str(file_t) + "_right"
            trial_type[(str(file_t)+"_right")] = data[file_t]
    return trial_type

def save_processed_files(data):
    '''
    Input:  Dictionary with the name of the individual files, 
            and the processed data (such that it is already resampled,
            filtered and selected the C3 and C4 channels)
    Function:   Save the processed data as a fif file for further analysis
    Output: list of saved file names and files safed in the current directory 
    '''
    df_data = data.keys()
    saved_files_list = []
    for file in df_data:
        name_f = str(file) + ".fif"
        data[file].save(name_f)
        saved_files_list.append(name_f)
    return saved_files_list
    
def save_trials(croped_files, boundaries_crop):
    a = croped_files.keys()
    for i in a: 
        a = "trials_"+i
        os.mkdir(a)
        os.chdir(a)
        trials_rl = right_left_tiral(croped_files[i], boundaries_crop[i][2], boundaries_crop[i][3])
        print(trials_rl)
        save_processed_files(trials_rl)
        os.chdir(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing")
    return 