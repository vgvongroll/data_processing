
import matplotlib.pyplot as plt
import pandas as pd
import scipy.signal as signal
from os import path

''' Loop to access individual data '''
def PSD_periodogram_fft(trials_dict):
    """
    Input:  Get a dictionary with the data from one run
    Function:   calculate PSD values with signal.periodogram
    Output: pd.DataFrame, with each trial as a row
    """
    columns = ["trial_name", "channel", "type_info"]
    columns.extend(list(range(376))) #MI task == 376
    df = pd.DataFrame(columns=columns)

    columns1 = ["trial_name", "channel", "type_info"]
    columns1.extend(list(range(314))) #rest == 314
    df1 = pd.DataFrame(columns=columns1)

    key_right = []
    key_left = []
    MI_task = [key_left, key_right]
    for task in trials_dict.keys():
        if "_right_" in task:
            key_right.append(task)
        elif "_left_" in task:
            key_left.append(task)
    for i in range(len(MI_task)):
        for trial in MI_task[i]:
            a, b = signal.periodogram(trials_dict[trial][0][0], fs = 250)
            C3 = str(trial)
            a = list(a)
            b= list(b[0])
            a.insert(0, "frequencies")
            a.insert(0, "C3")
            a.insert(0, C3)
            b.insert(0, "PSD")
            b.insert(0, "C3")
            b.insert(0, C3)

            c, d = signal.periodogram(trials_dict[trial][1][0], fs = 250)
            C4 = str(trial)
            c = list(c)
            d = list(d[0])
            c.insert(0, "frequencies")
            c.insert(0, "C4")
            c.insert(0, C4)
            d.insert(0, "PSD")
            d.insert(0, "C4")
            d.insert(0, C4)

            try:
                df.loc[len(df)] = a
                df.loc[len(df)] = b
                df.loc[len(df)] = c
                df.loc[len(df)] = d
            except:
                try:
                    df1.loc[len(df1)] = a
                    df1.loc[len(df1)] = b
                    df1.loc[len(df1)] = c
                    df1.loc[len(df1)] = d
                except:
                    print(len(a))
    return  df, df1

# getting a DF per run
def PSD_run(run_MI_rest):
    PSD_per_run = {}
    for run in run_MI_rest.keys():
        df, _ = PSD_periodogram_fft(run_MI_rest[run][0])
        _, df1 = PSD_periodogram_fft(run_MI_rest[run][1])
        PSD_per_run[run] = (df, df1)
    return PSD_per_run

"""
PLOT THE PSD OF THOSE TRIALS THAT HAVE A NEGATIVE MSI 
"""
#%%
# get the values form C3 and C4 for MI and for rest and plot the PSD values 
# to try to find a reason for the negative values 

def PSD_values_plot(df_MI, df_rest, trial_number):
    df_trial_MI = df_MI[df_MI["trial_name"].str.contains(trial_number, case=False)]
    df_trial_rest = df_rest[df_rest["trial_name"].str.contains(trial_number, case=False)]
    df_MI = df_trial_MI.iloc[:4]
    df_rest = df_trial_rest.iloc[:4]
    a = df_MI.iloc[:, 3:]
    b = df_rest.iloc[:, 3:]
    
    # first two lsit in list are from the C3 channel the other two are from C4
    # netsted list has the values for MI and rest 
    Row_list = []
    for _ in range(4):
        PSD_list_MI = list(a.iloc[_])
        PSD_list_rest = list(b.iloc[_])
        Row_list.append([PSD_list_MI, PSD_list_rest])
    return Row_list

def PSD_plot(Row_list):
    fig, axs = plt.subplots(2, 2)

    axs[0, 0].plot(Row_list[0][0], Row_list[1][0])
    axs[0, 0].set_title("PSD MI C3")
    axs[0, 0].set_ylabel("PSD [V**2/Hz]")
    axs[0, 0].set_xlabel("frequency Hz")
    axs[0, 0].set_xlim([0, 20])

    axs[0, 1].plot(Row_list[2][0], Row_list[3][0])
    axs[0, 1].set_title("PSD MI C4")
    axs[0, 1].set_ylabel("PSD [V**2/Hz]")
    axs[0, 1].set_xlabel("frequency Hz")
    axs[0, 1].set_xlim([0, 20])

    axs[1, 0].plot(Row_list[0][1], Row_list[1][1])
    axs[1, 0].set_title("PSD Rest C3")
    axs[1, 0].set_ylabel("PSD [V**2/Hz]")
    axs[1, 0].set_xlabel("frequency Hz")
    axs[1, 0].set_xlim([0, 20])

    axs[1, 1].plot(Row_list[2][1], Row_list[3][1])
    axs[1, 1].set_title("PSD Rest C4")
    axs[1, 1].set_ylabel("PSD [V**2/Hz]")
    axs[1, 1].set_xlabel("frequency Hz")
    axs[1, 1].set_xlim([0, 20])

    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.subplots_adjust()
    return fig

def plot_MSI_negative(PSD_per_run, run, trial_number):
    data_MI = PSD_per_run[run][0]
    data_rest = PSD_per_run[run][1]
    plot_values = PSD_values_plot(data_MI, data_rest, str(trial_number[:2]))
    fig = PSD_plot(plot_values)
    return fig

def plots_negative_trials(df, PSD_per_run):
    outpath = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI"
    neg = df[df["MSI"] < 0]
    run_keys = PSD_per_run.keys()
    fig_list = []
    #print(list(neg["run"]))
    for runk in run_keys:
        for (sub, run, trial) in zip(list(neg["subject"]), list(neg["run"]), list(neg["trial"])):
            key_run = str(sub)+"_"+str(run)
            if key_run == runk[7:]:
                #print(key_run)
                #print(run, sub, trial)
                fig = plot_MSI_negative(PSD_per_run, runk, trial)
                fig_list.append(fig)
                #name_plot = str(runk[7:]) + str(trial[:7]) + ".pdf"
                #fig.savefig(path.join(outpath, name_plot))
    return fig_list
