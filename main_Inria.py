
#%%
from load_gdf_files import load_raw_mne, path_file_Inria
from events_gdf import get_events
from resampling_bandpass import channels, filter_bandpass, resample_250
from save_processed_data import save_processed_files, save_trials
from load_processed_files import load_processed_mne, path_file_processed_data, path_file_trials, multiple_subects_pd
from segmentation import crop_multiple, crop_MI_rest
from mu_suppression import get_PSD_values, MSI_per_run, MSI_per_subject
from negative_MSI import PSD_run, plots_negative_trials

#%%
"""
THIS FILE IS THE SCRIPT USED TO PERFORM THE WHOLE DATA ANALYSIS FROM 
THE INRIA DATASET
IT FOLLOWS THE WHOLE PIPELINE 
"""

#%%
''' Load the files '''
# Get the path from the files
# load the files and convert them into Raw MNE objects 
from load_gdf_files import load_raw_mne, path_file_Inria
path_A = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\Inria\DATA A"
path_B = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\Inria\DATA B"
path_C = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\Inria\DATA C"
#%%
inria_A = load_raw_mne(path_file_Inria(path_A))
#%%
inria_B = load_raw_mne(path_file_Inria(path_B))
#%%
inria_C = load_raw_mne(path_file_Inria(path_C))
#%%
print(len(inria_C))
key_inria1 = list(inria_C.keys())
inria1 = inria_C
#%%
''' Get the events from the data '''
from events_gdf import get_events
key_inria_A = list(inria_A.keys())
key_inria1 = key_inria_A[:150]
key_inria2 = key_inria_A[150:300]
key_inria3 = key_inria_A[300:]
#key_inria_B = list(inria_B.keys())
#key_inria_C = list(inria_C.keys())

#%%
inria1 = {}
for _ in key_inria3:
    inria1[_] = inria_A[_]
#print(inria1)

#%%
# Adjust the folder from the files A, B, C
events_inria = {}
for file in key_inria1:
    events, event_dict = get_events(inria1[file])
    events_inria[file] = [events, event_dict]
# print(len(events_inria))
# Create a dictionary for the events meaning in the Inria dataset
event_dict_words = {"1010":1, "3":2, "Experiment_starts":3, "33281":4, 
                    "Beep_sound":5, "Trial_starts":6, "Left_hand_MI":7, 
                    "Right_hand_MI": 8, "continuous_feedback": 9, 
                    "appearance_green_cross":10 ,"end_of_trial": 11}

#%%
""" INSPECT THE INDIVIDUAL TRIALS """
''' Figure out wether to reject some trials '''
# The code for it is still missging, It needs to reject the trials that 
# have a higher value than 100 mV
""" APPLY A SPATIAL FILTER """
# Apply a spatial filter in oerder to have cleaner data
# the code is still not there
# first it is immportant to compare the whether applying the filter 
# improves the MSI values
 
#%%
""" PRE-PROCESSED THE INDIVIDUAL FILES """
''' Resampling, fitering, selecting channels '''
# Adjust the folder from the files A, B, C
from resampling_bandpass import channels
ch_inria = channels(inria1)
#%%
from resampling_bandpass import filter_bandpass
filtered_inria = filter_bandpass(ch_inria)
#%%
from resampling_bandpass import resample_250
resampled_inria = resample_250(filtered_inria)

#%%
''' Save the processed data into files '''
# allowes to do the previous steps only onve for each folder
from save_processed_data import save_processed_files
names_sppd = save_processed_files(resampled_inria)





#%%
""" LOAD PRE-PROCESSED DATA """
from load_processed_files import load_processed_mne, path_file_processed_data
# Check whether to do this separately for each folder or as one dataset 
# As one data set would be best   
path_pd = r'C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_dataset' 

inria_processed = load_processed_mne(path_file_processed_data(path_pd))
key_inria_p = list(inria_processed.keys())

#%%
from events_gdf import get_events
events_inria = {}
for file in key_inria_p:
    events, event_dict = get_events(inria_processed[file])
    events_inria[file] = [events, event_dict]
print(len(events_inria))

#%%
''' SEGMENT INDIVIDUAL TRIALS '''
''' create a loop that does all rhis steps for each file '''
from segmentation import crop_multiple
boundaries_crop, croped_files = crop_multiple(key_inria_p, events_inria, inria_processed)
#print(croped_files)

#%%
''' Save the the devided files (into individual trials) in folder'''
from save_processed_data import save_trials
save_trials(croped_files, boundaries_crop)





#%%
""" CALCULATE THE MU-SUPPRESSION VALUES """
''' Load the individual trials '''
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_saved_trials = multiple_subects_pd(path_file_trials())

# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = inria_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(inria_saved_trials[run])
    run_dict[run] = loaded_tials

#%%
''' Segment trial into MI and Rest '''
from segmentation import crop_MI_rest
key_runs = run_dict.keys()
run_MI_rest = {}
failed_run = {}
for run in key_runs:
    trial_MI, trial_rest, failed_files = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]
    failed_run[run] = failed_files
# apparently the trial C86 presented some problems, 
# specifically runs: R3_onlineT, R4_onlineT, R5_onlineT, R6_onlineT

#%%
for i in failed_run.keys():
    if len(failed_run[i]) > 0:
        print(i)
        print(failed_run[i])

#%%
''' Create pandas dataframe with individual MSI per trial '''
from mu_suppression import get_PSD_values
df = get_PSD_values(run_MI_rest=run_MI_rest)
df

#%%
''' Crate pandas dataframe with averaged MSI for each run '''
from mu_suppression import MSI_per_run
df_run = MSI_per_run(df)
df_run

#%%
''' Create pandas dataframe with averaged MSI for each subject'''
from mu_suppression import MSI_per_subject
df_avg = MSI_per_subject(df)
df_avg

#%%
# Save the DF into CSV files in order to access them later 
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\PSD.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_run.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_run.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_avg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_avg.to_csv(filepath)

#%%
""" TO PLOT PSD VALUES FOR ROWS IN DATAFRAME """
neg = df[df["MSI"] < 0]
neg
#%%
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_MSI\MSI_neg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
neg.to_csv(filepath)
#%%
from negative_MSI import PSD_run
PSD_per_run = PSD_run(run_MI_rest)

#%%
from negative_MSI import plots_negative_trials
fig_list = plots_negative_trials(df, PSD_per_run)
