
READ_ME 
**There is the possibility to imprve the code using** 
    - classes (file type, that has it individual methods in order to be pre-processed)
    - once it is preprocessed and saved in the right format, normal functions can be used to calculate the mu-suppression values
    - functional programming

**LOAD_GDF_FILES.PY** 
    - dictionary 
        - file name 
        - raw_mne loaded 

- from load_gdf_files import load_raw_mne, path_file_Inria
- inria = load_raw_mne(path_file_Inria())
- inria_df = inria.keys()

**LOAD_CSV_FILES.PY**
    - dictionary
        - file name
        - pandas dataframe loaded files

- from load_csv_files import load_csv, path_file_PCF
- pcf = load_csv(path_file_PCF())
- pcf_df = pcf.keys()

    **MNE_CONVERTION.PY**
        - dictionary 
            - file name
            - raw_mne loaded 

    - from mne_convertion import convert_csv_mne
    - raw_pcf = convert_csv_mne(pcf)
        - (can use MNE PACKAGE)

**PATH_MAT_FILES.PY**
    - list file paths

- from path_mat_files import path_file_mat
- emb_p = path_file_mat()

**LOAD_MAT_PYTHON.PY** (*this was taken from stackoverflow*, need to be cited)
    - dictionary
        - matlab variable
        - array 

- from load_mat_python import loadmat, _check_keys, _todict
- emb = _check_keys(loadmat(emb_p[1]))
- emb_df = emb['eeg'].keys()
- print(emb_df)

**MNE PACKAGE**
- <loaded_file>.info
- <loaded_file>.resample(<Hz_frequency>)
- <loaded_file>.plot()
- <loaded_file>.plot_psd()
    - import matplotlib.pyplot as plt
    - plt.pause(10)

'''
It might be possible to create one function that creates the paths 
    - it can find the folder based on file names
    - it decided if to go into other folder looking for the data '
    - depending on data format does different steps 
'''

'''
GDF files are in raw_mne
CSV files are in raw_mne

MAT files (still need to be processed into raw_mne)

Create function that resamples all data into 250Hz and stores them in an extra file

Create function that conducts a amplitude analysis 
    - decides whether to keep trial or reject it 

Create function that compares if there is a difference
in PSD if spatial filter is implemented 

Create a script that analises all the data to start calculating 
    - mu suppression 