
'''
This is the script to do the whole pipline up to the 
MSI values for the Inria Dataset 
'''
#%%
''' Load the files  (GDF INRIA)'''
# GET PATH, CONVERT INTO RAW MNE 
from load_gdf_files import load_raw_mne, path_file_Inria
inria = load_raw_mne(path_file_Inria())
#%%
key_inria = list(inria.keys())
key_inria1 = key_inria[:30]
key_inria2 = key_inria[30:50]
key_inria3 = key_inria[50:]
print(key_inria)

#%%
inria_30 = {}
for _ in key_inria1:
    inria_30[_] = inria[_]
print(inria_30)
''' GET THE EVENTS FROM THE DATA, '''
#%%
from events_gdf import get_events
events_inria = {}
for file in key_inria1:
    events, event_dict = get_events(inria_30[file])
    events_inria[file] = [events, event_dict]
print(len(events_inria))
#%% 
print(events_inria['A1_CE_baseline'][1])
print(events_inria['A13_R1_acquisition'][1])
raw = inria['A1_CE_baseline']
raw.plot()
raw.plot(events=events, start=5, duration=10, color='gray')

# %%
# Dictionary for events on the Inria dataset 
import mne
event_dict_words = {"1010":1, "3":2, "Experiment_starts":3, "33281":4, "Beep_sound":5, "Trial_starts":6, 
                    "Left_hand_MI":7, "Right_hand_MI": 8, "continuous_feedback": 9, 
                    "appearance_green_cross":10 ,"end_of_trial": 11}
#%%
'''
# plots the event id and the time at which it appears 
# it is creating some errors when information is missing 
for file in key_inria[4:]:
    fig = mne.viz.plot_events(events_inria[file][0], event_id=event_dict_words, sfreq=inria[file].info['sfreq'],
                          first_samp=inria[file].first_samp)
'''
#%%
''' INSPECT '''
''' Check what it does with the rejeceted trials, can i 
delete the merg repeated '''
import mne
reject_criteria = dict(eeg = 100e-6) # 100 micro volt 
epochs = mne.Epochs(raw, events = events, reject_tmax=0,
                    reject=reject_criteria, reject_by_annotation=True, 
                    preload=True, event_repeated = "merge")
epochs.plot_drop_log()

'''
print(raw.annotations)
print(len(raw.annotations))
print(set(raw.annotations.duration))
print(set(raw.annotations.description))
print(raw.annotations.onset[0])
mne.find_events(raw)
'''

''' SPATIAL FILTER '''




''' PRE-PROCESS THE DIFFERENT FILES (RESAMPLING, FILTER, SELECT CHANNELS)'''
# %%
from resampling_bandpass import channels
ch_inria = channels(inria_30)
#%%
from resampling_bandpass import filter_bandpass

filtered_inria = filter_bandpass(ch_inria)

#%%
from resampling_bandpass import resample_250
resampled_inria = resample_250(filtered_inria)
#%%
inria_processed = resampled_inria


''' SAVE THE PROCESSED DATA INTO FILES '''
#%%
from save_processed_data import save_processed_files
names_sppd = save_processed_files(resampled_inria)
# %%
# print(names_sppd)

#%%
''' LOAD PRE-PROCESSED DATA'''
#%%
from load_processed_files import load_processed_mne, path_file_processed_data
inria_processed = load_processed_mne(path_file_processed_data())
key_inria_p = inria_processed.keys()
#%%
key_inria = list(inria_processed.keys())
key_inria1 = key_inria[:30]

from events_gdf import get_events
events_inria = {}
for file in key_inria1:
    events, event_dict = get_events(inria_processed[file])
    events_inria[file] = [events, event_dict]
print(len(events_inria))
#%%
print(len(key_inria_p))
print(inria_processed['A60_R4_onlineT'].info)
inria_processed['A60_R4_onlineT'].plot()

#%%
''' SEGMENT INDIVIDUAL TRIALS '''
''' create a loop that does all rhis steps for each file '''
#from segmentation import segmentation_boundaries_gdf, crop_trials, 
from segmentation import crop_multiple
boundaries_crop, croped_files = crop_multiple(key_inria1, events_inria, inria_processed)

print(croped_files)

#%%
# Save files devided into trials into one folder 
from save_processed_data import save_trials
save_trials(croped_files, boundaries_crop)



"""  
The following part was copied into the PSD_plots file 
to check if it is the best way and to work on the individual
code for all the ways to cmpute and plot PSD values
"""
#%%
''' SEGMENT INTO (MI AND REST) '''
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_saved_trials = multiple_subects_pd(path_file_trials())

#%%
# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = inria_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(inria_saved_trials[run])
    run_dict[run] = loaded_tials
#%%
# segment all trials into MI and rest 
key_runs = run_dict.keys()
run_MI_rest = {}
for run in key_runs:
    from segmentation import crop_MI_rest
    trial_MI, trial_rest = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]

#%%
#print(run_MI_rest.keys())
keys_MI = run_MI_rest['trials_A10_R1_acquisition'][0].keys()
keys_rest = run_MI_rest['trials_A10_R1_acquisition'][1].keys()
print(keys_MI)
print(keys_rest)
#%%
# devide the data into individual channels (0) = C3 and (1) = C4
# how ro acces each individual file 
import numpy as np
run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'].plot()
run_MI_rest['trials_A10_R1_acquisition'][1]['10_left_rest'].plot()
#%%
# the raw file has one tuple pro channel, 
# each tuple is created by two arrays
#   array 0 = the amplitude value 
#   array 0 = time axis 
# You can access them by indexing twice
print(run_MI_rest['trials_A10_R1_acquisition'][1]['10_left_rest'][0][0])
print(run_MI_rest['trials_A10_R1_acquisition'][1]['10_left_rest'][0][1])


#%%
''' CALCULATE PSD WITH import scipy.fft as fft'''
import scipy.fft as fft
import matplotlib.pyplot as plt
#fft.fft(run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'])
a = fft.fft(run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'][0][0])
plt.plot(run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'][0][1], a[0])
plt.show()

#%%
import scipy.fft as fft
import matplotlib.pyplot as plt
#fft.fft(run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'])
a = fft.fft(run_MI_rest['trials_A10_R1_acquisition'][1]['10_left_rest'][0][0])
plt.plot(run_MI_rest['trials_A10_R1_acquisition'][1]['10_left_rest'][0][1], a[0])
plt.show()

#%%
''' CALCULATE PSD WITH SCIPY '''
import scipy.signal as signal 
b, c = signal.periodogram(run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'][0][0], fs = 250)
#print(b)
#print(c)
#plt.semilogy(b, c[0])
plt.plot(b, c[0])
plt.xlim([0, 20])
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD [V**2/Hz]')
plt.show()

#%%
''' Plot the PSD from a fft '''
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

data = run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'][0][0]
ps = np.abs(np.fft.fft(data))**2

time_step = 1 / 250
freqs = np.fft.fftfreq(data.size, time_step)
idx = np.argsort(freqs)

plt.plot(freqs, ps[0])
plt.xlim([0, 20])
#%%
''' CALCULATE PSD_WELCH '''
a = run_MI_rest['trials_A10_R1_acquisition'][0]
from welch_psd import welch_psd_values
psds_MI = welch_psd_values(a)
#psds_rest = welch_psd_values(trial_rest)

# %%
from welch_psd import average_over_band
av_psds_MI = average_over_band(psds_MI)
#av_psds_rest = average_over_band(psds_rest)
#print(av_psds_MI)
#print(av_psds_rest)
# %%
from welch_psd import classify_trials
l_psds_MI, r_psds_MI = classify_trials(av_psds_MI)
#l_psds_rest, r_psds_rest = classify_trials(av_psds_rest)

print(l_psds_MI)
print(r_psds_MI)
