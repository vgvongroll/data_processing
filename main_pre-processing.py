
### This main file is going to be used to get all the fucntions needed 
### once the preprocessing has been done. 

#%%
from load_processed_files import load_processed_mne, path_file_processed_data
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_processed = load_processed_mne(path_file_processed_data())
# %%
print(inria_processed.keys())
# %%
import matplotlib as plt
import mne
a = ['A9_R4_onlineT', 'A9_R6_onlineT']
inria_processed['A9_R6_onlineT'].plot()
inria_processed['A9_R6_onlineT'].plot_psd()
# %%
raw = inria_processed['A9_R6_onlineT']

#%%
import mne
events, event_dict = mne.events_from_annotations(raw)
print(events)
print(event_dict)

#%%
print(raw.annotations)
print(len(raw.annotations))
print(set(raw.annotations.duration))
print(set(raw.annotations.description))
print(raw.annotations.onset[0])
#mne.find_events(raw)

#%%
import mne
reject_criteria = dict(eeg = 100e-6) # 100 micro volt 
epochs = mne.Epochs(raw, events = events, reject_tmax=0,
                    reject=reject_criteria, reject_by_annotation=True, 
                    preload=True, event_repeated = "merge")
epochs.plot_drop_log()

#%%
epochs.plot()
# %%
fig = mne.viz.plot_events(events, event_id=event_dict, sfreq=raw.info['sfreq'],
                          first_samp=raw.first_samp)
# %%
# Dictionary for events on the Inria dataset 
event_dict_words = {"1010":1, "3":2, "Experiment_starts":3, "33281":4, "Beep_sound":5, "Trial_starts":6, 
                    "Left_hand_MI":7, "Right_hand_MI": 8, "continuous_feedback": 9, 
                    "appearance_green_cross":10 ,"end_of_trial": 11}
fig = mne.viz.plot_events(events, event_id=event_dict_words, sfreq=raw.info['sfreq'],
                          first_samp=raw.first_samp)
# %%
print(events.shape())