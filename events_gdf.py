
import matplotlib.pyplot as plt
import mne
from load_csv_files import load_csv, path_file_PCF
from mne_convertion import convert_csv_mne


def get_events(raw_data):
    '''
    Inputs:
    Function:
    Outputs:
    '''
    events, event_dict = mne.events_from_annotations(raw_data)
    return events, event_dict

# select channels from PCF dataset to create annotated data 
# and the events afterwords \
def load_events():
    ''' Load the files '''
    # Get the path from the PCF files  
    # load the csv files and convert them into Raw MNE objects
    path_PCF = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\psychological_cognitive_factors\Raw eeg 2"
    pcf = load_csv(path_file_PCF(path_PCF))

    ''' Convert csv into Raw MNE '''
    # get basic info from raw_csv
    raw_pcf = convert_csv_mne(pcf)
    return raw_pcf

def channels_annotation(data):
    '''
    Input:
    Function:
    Output:
    '''
    data_channels = {}
    df_data = data.keys()
    for file in df_data:
        data_channels[file] = data[file].copy().pick_channels(["TimeStamp", "trial", "class"])
    return data_channels
