# This file is the main file to process the individual functions that are in charge about segementing the files
# Trials
#   rest 
#   MI
#   left
#   right

# %%
'''
LOAD GDF FILES 
'''
# load GDF files, 
#   get the file path and load all files as raw mne 
from numpy import average
from load_gdf_files import load_raw_mne, path_file_Inria
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria = load_raw_mne(path_file_Inria())
# inria_df = inria.keys()
# print(inria.keys())

#%%
"""
SEGMENT INDIVIDUAL TRIALS 
"""
# example with one file

import mne
'''
keys_list = ['A9_CE_baseline', 'A9_OE_baseline', 'A9_R1_acquisition', 'A9_R2_acquisition', 'A9_R3_onlineT', 'A9_R4_onlineT', 'A9_R5_onlineT', 'A9_R6_onlineT'] 
raw = inria['A9_R6_onlineT']
print(raw.info)
'''
raw = inria["A9_R5_onlineT"].pick_channels(["C3", "C4"])
raw = raw.filter(l_freq = 8, h_freq = 13)
raw.info
#raw = raw.resample(250)

#%%
# extract the annotations from the file and convert 
# them into events to use them as a boundary for the trials 
events, event_dict = mne.events_from_annotations(raw)
print(events)
print(len(events))
print(event_dict)
#print(events[:20])

print(raw.annotations)
print(len(raw.annotations))
print(set(raw.annotations.duration))
print(set(raw.annotations.description))
print(raw.annotations.onset[0])
#mne.find_events(raw)

#%%
# reject channels based on the boundary 100mv

# i am still unclear how this works and if i should use it
import mne
reject_criteria = dict(eeg = 100e-6) # 100 micro volt 
epochs = mne.Epochs(raw, events = events, reject_tmax=0,
                    reject=reject_criteria, reject_by_annotation=True, 
                    preload=True, event_repeated = "merge")
epochs.plot_drop_log()
# epochs.plot()
# fig = mne.viz.plot_events(events, event_id=event_dict, sfreq=raw.info['sfreq'],
#                          first_samp=raw.first_samp)

# Dictionary for events on the Inria dataset 
event_dict_words = {"1010":1, "3":2, "Experiment_starts (32769)":3, "33281":4, 
                    "Beep_sound (33282)":5, "Trial_starts (768)":6, "Left_hand_MI (769)":7,
                    "Right_hand_MI (770)": 8, "continuous_feedback (781)": 9, 
                    "appearance_green_cross (786)":10 ,"end_of_trial (800)": 11}
fig = mne.viz.plot_events(events, event_id=event_dict_words, sfreq=raw.info['sfreq'],
                          first_samp=raw.first_samp)

#%%
raw.plot(events=events, start=5, duration=10, color='gray')

# %%
# This should be a function that uses the events 
# that are annotated and stroe them in different 
# dictionaries. those can be used to segment the data into 
# different trials, MI, rest and left, right
from segmentation import segmentation_boundaries_gdf, crop_trials
trials_start, trials_stop, trials_left, trials_right = segmentation_boundaries_gdf(events)

#%%)
croped_file_trials = crop_trials(raw, trials_start, trials_stop)

#%%
print(len(raw))
print(trials_start)
print(trials_stop)
#%% 
a = croped_file_trials.keys()
print(len(a))
for i in a:
    print(croped_file_trials[i].plot())
    # print(croped_file_trials[i].info)
    print(trials_start[i], trials_stop[i])
    print(len(croped_file_trials[i]))
    # print(croped_file_trials[i].plot_psd())

#%%
# Save the files for each trial
from save_processed_data import save_processed_files, right_left_tiral
trials_rl = right_left_tiral(croped_file_trials, trials_left, trials_right)
save_processed_files(trials_rl)

#%% 
print(trials_stop)
print(len(raw))
print(raw.info)
raw.plot()
# %%
print(raw[0])
print(inria['A9_R5_onlineT'].info)
print(inria['A9_R5_onlineT'].plot())
raw.plot_psd()
raw.info
print(events)
raw.plot()


#%%
"""
SEGMENTATION INTO 
    - MI
    - REST
"""
"""
This is loading the saved data and ploting the indivisual trials 
"""
from load_processed_files import load_processed_mne, path_file_trials
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_processed = load_processed_mne(path_file_trials())
# %%
trials_key = inria_processed.keys()
print(len(inria_processed.keys()))
print(trials_key)

for i in trials_key:
    print(inria_processed[i])
    # inria_processed[i].plot_psd()
    inria_processed[i].plot()


# %%
"""
The actual segementation into MI and rest
"""
from segmentation import crop_MI_rest
trial_MI, trial_rest = crop_MI_rest(inria_processed)