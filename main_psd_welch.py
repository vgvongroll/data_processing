
'''
Load and segemet data into MI and rest 
    code form main_Segementing_data
'''
#%%
from load_processed_files import load_processed_mne, path_file_trials
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_processed = load_processed_mne(path_file_trials())

from segmentation import crop_MI_rest
trial_MI, trial_rest = crop_MI_rest(inria_processed)

'''
Functions
'''
#%%
from welch_psd import welch_psd_values
psds_MI = welch_psd_values(trial_MI)
psds_rest = welch_psd_values(trial_rest)

# %%
from welch_psd import average_over_band
av_psds_MI = average_over_band(psds_MI)
av_psds_rest = average_over_band(psds_rest)
print(av_psds_MI)
print(av_psds_rest)
# %%
from welch_psd import classify_trials
l_psds_MI, r_psds_MI = classify_trials(av_psds_MI)
l_psds_rest, r_psds_rest = classify_trials(av_psds_rest)

print(l_psds_MI)
print(r_psds_MI)
# %%
'''
l_subject_rest = average_per_subject(l_psds_rest)
r_subject_rest = average_per_subject(r_psds_rest)
l_subject_MI = average_per_subject(l_psds_MI)
r_subject_MI = average_per_subject(r_psds_MI)

print(r_subject_MI, r_subject_rest)
print(l_subject_MI, l_subject_rest)
'''

#%%
'''
Calculate Mu suppression values 
'''
print(l_psds_MI)
#print(r_psds_MI)
print(l_psds_rest)
#print(r_psds_rest)
# %%
ERD = l_psds_MI["10_left_MI"][0] - l_psds_rest["10_left_rest"][0]
print(ERD)
print(l_psds_MI["10_left_MI"][1] - l_psds_rest["10_left_rest"][1])
# %%
