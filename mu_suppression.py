
import numpy as np
import scipy.signal as signal
import pandas as pd

"""
Functions to calculate the Mu-suppression index 
"""

def get_PSD_values(run_MI_rest, dataset = "Inria"):
    '''
    Input:  Dictionary, with the run, trial,...
    Function:   Create a pandas.DataFrame that has the 
                average PSD value for each group
                group: (MI C3, MI C4, Rest C3, Rest C4)
    Output: DF with PSD values per trial 
    '''
    # SUBJECT, RUN, TRIAL, LEFT/RIGHT, (4X DIVISIONS), MSI
    columns = ["dataset","subject", "run", "task", "trial", "MI C3", "MI C4", "Rest C3", "Rest C4", "MSI_C3_C4", "MSI" ]
    df = pd.DataFrame(columns=columns)
    
    # Loop over the runs and extract all groups 
    for run in run_MI_rest.keys():
        row_per_trial = [dataset, run[7:10], run[11:], "task", "trial", "MI C3", "MI C4", "Rest C3", "Rest C4", "MSI_C3_C4", "MSI"]
        # loop over the MI and rest 
        '''
        for trial in range(len(run_MI_rest[run])):
            if trial == 0:
                row_per_trial.append("MI")
            elif trial == 1:
                row_per_trial.append("Rest")    
        '''     
        #  Loop over trial_name and access the trial data
        for (trial_MI, trial_rest) in zip(run_MI_rest[run][0].keys(), run_MI_rest[run][1].keys()):
            
            # compute PSD value for individual raw data
            a, b = signal.periodogram(run_MI_rest[run][0][trial_MI][0][0], fs = 250)
            c, d = signal.periodogram(run_MI_rest[run][0][trial_MI][1][0], fs = 250)
            row_per_trial[5] = np.average(b)
            row_per_trial[6] = np.average(d)
            a, b = signal.periodogram(run_MI_rest[run][1][trial_rest][0][0], fs = 250)
            c, d = signal.periodogram(run_MI_rest[run][1][trial_rest][1][0], fs = 250)
            row_per_trial[7] = np.average(b) #[24:40])
            row_per_trial[8] = np.average(d)

            if "_right_" in trial_MI:
                row_per_trial[3] = "right"
                row_per_trial[9], row_per_trial[10] = MSI(row_per_trial[3], row_per_trial[5], row_per_trial[6], row_per_trial[7], row_per_trial[8])
            elif "_left_" in trial_MI:
                row_per_trial[3] = "left"
                row_per_trial[9], row_per_trial[10] = MSI(row_per_trial[3], row_per_trial[5], row_per_trial[6], row_per_trial[7], row_per_trial[8])
            row_per_trial[4] = trial_MI

            # Append row of df
            # Set ("task", "trial", "MI C3", "MI C4", 
            # "Rest C3", "Rest C4") to 0
            df.loc[len(df)] = row_per_trial
            '''for i in range(3, len(row_per_trial)):
                row_per_trial[i] = 0
            '''
    return df

def get_PSD_values_EEG(run_MI_rest, task, dataset = "EEG_MI_BCI"):
    '''
    Input:  Dictionary, with the run, trial,...
    Function:   Create a pandas.DataFrame that has the 
                average PSD value for each group
                group: (MI C3, MI C4, Rest C3, Rest C4)
    Output: DF with PSD values per trial 
    '''
    # SUBJECT, RUN, TRIAL, LEFT/RIGHT, (4X DIVISIONS), MSI
    columns = ["dataset","subject", "task", "trial", "MI C3", "MI C4", "Rest C3", "Rest C4", "MSI_C3_C4", "MSI" ]
    df = pd.DataFrame(columns=columns)
    
    # Loop over the runs and extract all groups 
    for run in run_MI_rest.keys():
        row_per_trial = [dataset, run, task, "trial", "MI C3", "MI C4", "Rest C3", "Rest C4", "MSI_C3_C4", "MSI"]
        #  Loop over trial_name and access the trial data
        for (trial_MI, trial_rest) in zip(run_MI_rest[run][0].keys(), run_MI_rest[run][1].keys()):
            
            # compute PSD value for individual raw data
            a, b = signal.periodogram(run_MI_rest[run][0][trial_MI][0][0], fs = 250)
            c, d = signal.periodogram(run_MI_rest[run][0][trial_MI][1][0], fs = 250)
            row_per_trial[4] = np.average(b)
            row_per_trial[5] = np.average(d)
            a, b = signal.periodogram(run_MI_rest[run][1][trial_rest][0][0], fs = 250)
            c, d = signal.periodogram(run_MI_rest[run][1][trial_rest][1][0], fs = 250)
            row_per_trial[6] = np.average(b) #[24:40])
            row_per_trial[7] = np.average(d)

            if task == 'right':
                row_per_trial[2] = "right"
                row_per_trial[8], row_per_trial[9] = MSI(row_per_trial[2], row_per_trial[4], row_per_trial[5], row_per_trial[6], row_per_trial[7])
            elif task == "left":
                row_per_trial[2] = "left"
                row_per_trial[8], row_per_trial[9] = MSI(row_per_trial[2], row_per_trial[4], row_per_trial[5], row_per_trial[6], row_per_trial[7])
            row_per_trial[3] = trial_MI

            # Append row of df
            # Set ("task", "trial", "MI C3", "MI C4", 
            # "Rest C3", "Rest C4") to 0
            df.loc[len(df)] = row_per_trial
            '''for i in range(3, len(row_per_trial)):
                row_per_trial[i] = 0
            '''
    return df


def MSI(task, pMI_C3, pMI_C4, prest_C3, prest_C4):
    '''
    Input: right or left MI task, and the four PSD values (MI and rest) (C3 and C4)
    Function: compute the mu suppression index per subject
    Output: Mu sippression index depending if right or left MI task
    '''
    ERD_C3 = pMI_C3 - prest_C3
    ERD_C4 = pMI_C4 - prest_C4
    
    ERS_C3 = prest_C3
    ERS_C4 = prest_C4
    if task == "right":
        MSI_right = (ERD_C4/ERS_C4) - (ERD_C3/ERS_C3)
        MSI = ((ERD_C3/ERS_C3), (ERD_C4/ERS_C4))
        return MSI, MSI_right
    elif task == "left":
        MSI_left = (ERD_C3/ERS_C3) - (ERD_C4/ERS_C4)
        MSI = ((ERD_C3/ERS_C3), (ERD_C4/ERS_C4))
        return MSI, MSI_left

def MSI_per_subject(df):
    '''
    Input:  Dataframe, with all the MSI values for each trial for all 
            subjects
    Function:   Get all the MSI values per subject and average them 
    Output: Dataframe, with one MSI value for right and one for left tirals
    '''
    # should I also get the average per run and then per subject?!
    columns = ["dataset", "subject", "avg_MSI_left", "avg_MSI_right", "ovl_MSI"]
    df_avg = pd.DataFrame(columns=columns)
    datasets = set(df["dataset"])
    subjects = set(df["subject"])

    for dataset in datasets:
        for subject in subjects:
            row_per_subject = [dataset, subject, "avg_MSI_left", "avg_MSI_right", "ovl_MSI"]

            sub_df = df[df["subject"] == subject]
            left = sub_df[sub_df["task"] == "left"]
            right = sub_df[sub_df["task"] == "right"]
            MSI_l = np.average(list(left["MSI"]))
            MSI_r = np.average(list(right["MSI"]))
            row_per_subject[2] = MSI_l
            row_per_subject[3] = MSI_r
            row_per_subject[4] = MSI_l + MSI_r 

            df_avg.loc[len(df_avg)] = row_per_subject
    return df_avg

def MSI_per_subject(df):
    '''
    Input:  Dataframe, with all the MSI values for each trial for all 
            subjects
    Function:   Get all the MSI values per subject and average them 
    Output: Dataframe, with one MSI value for right and one for left tirals
    '''
    # should I also get the average per run and then per subject?!
    columns = ["dataset", "subject", "avg_MSI_left", "avg_MSI_right", "ovl_MSI"]
    df_avg = pd.DataFrame(columns=columns)
    datasets = set(df["dataset"])
    subjects = set(df["subject"])

    for dataset in datasets:
        for subject in subjects:
            row_per_subject = [dataset, subject, "avg_MSI_left", "avg_MSI_right", "ovl_MSI"]

            sub_df = df[df["subject"] == subject]
            left = sub_df[sub_df["task"] == "left"]
            right = sub_df[sub_df["task"] == "right"]
            MSI_l = np.average(list(left["MSI"]))
            MSI_r = np.average(list(right["MSI"]))
            row_per_subject[2] = MSI_l
            row_per_subject[3] = MSI_r
            row_per_subject[4] = MSI_l + MSI_r 

            df_avg.loc[len(df_avg)] = row_per_subject
    return df_avg

def MSI_per_run(df):
    '''
    Input:  Dataframe, with all the MSI values for each trial for all 
            subjects
    Function:   Get all the MSI values per subject and average them 
    Output: Dataframe, with one MSI value for right and one for left tirals
    '''
    # should I also get the average per run and then per subject?!
    columns = ["dataset", "subject", "run", "avg_MSI_left", "avg_MSI_right"]
    df_avg = pd.DataFrame(columns=columns)
    datasets = set(df["dataset"])
    subjects = set(df["subject"])
    runs = set(df["run"])

    for dataset in datasets:
        for subject in subjects:
            for run in runs:    
                row_per_subject = [dataset, subject, run, "avg_MSI_left", "avg_MSI_right"]

                sub_df = df[df["subject"] == subject]
                sub_run = sub_df[sub_df["run"] == run]
                left = sub_run[sub_run["task"] == "left"]
                right = sub_run[sub_run["task"] == "right"]
                MSI_l = np.average(list(left["MSI"]))
                MSI_r = np.average(list(right["MSI"]))
                row_per_subject[3] = MSI_l
                row_per_subject[4] = MSI_r

                df_avg.loc[len(df_avg)] = row_per_subject
    return df_avg