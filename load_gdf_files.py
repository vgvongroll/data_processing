
import mne
import os

# Getting file path 
# Use path to load files

def path_file_Inria(path_Inria):
    '''
    Input:  add the path to the gdf folders (can be added)
    Function:   creates a list of paths for every GDF file 
                (has an extra external loop to enter individual subjects folder 
                inside the given path) Files contain EEG signal
    Output:     retuns a list with the paths to all individual files
    '''
    # Path for Inria Dataset (Data A)
    #   Data B and C are missing
    # path_Inria = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\Inria\Data A"

    Inria_folder = os.listdir(path_Inria) # list the files that are at that directory
    # print(Inria_folder)

    Inria_file = []
    Inria_files = []
    for folder in Inria_folder:
        Inria_file = path_Inria + "\\" + folder
        # print(Inria_file)
        Inria_file_dir = os.listdir(Inria_file)
    
        for file in Inria_file_dir:
            if "gdf" in file:
                path = Inria_file + "\\" + file
                Inria_files.append(path)
    return Inria_files
    
# pprint(path_file_Inria())
# print(len(path_file_Inria()))

def load_raw_mne(path_list):
    '''
    Input:  A list with paths to individual gdf files
    Function:   save the loaded files in a dictionary 
                (key is the name of the file (without file extention),
                item is the loaded raw_mne file)
    Output:     Dictionary with file names and loaded raw_mne file
    '''
    raw_dic = {}
    for path in path_list:
        name = os.path.basename(path)
        raw_dic[name[:-4]] = mne.io.read_raw_gdf(path, preload= True)
    return raw_dic


