
import mne

def segmentation_boundaries_gdf(events):
    """
    Input:  the numpy array with the events
    Function:   Create dictionaries that safe the second at which a certain 
                event occurs. These can be used as boundaries for the different 
                segmentations
                    - trials
                    - rest vs MI
                    - right vs left
    Output: as many dictionaries as useful events, having the indice as a key and 
            the time in seconds from when it occurs  
    """
    dict_trials_start = {}
    dict_trials_stop = {}
    dict_trial_left = {}
    dict_trial_right = {}
    dict_start_MI = {}
    trial = 1
    for element in range(len(events)):
        if events[element][2] == 6:
            dict_trials_start[trial] = (events[element][0] / 250)
        elif events[element][2] == 11:
            dict_trials_stop[trial] = (events[element][0] / 250)
            trial += 1
        elif events[element][2] == 7:
            dict_trial_left[trial] = ((events[element][0] / 250)/ 10) #- dict_trials_start[trial]
        elif events[element][2] == 8:
            dict_trial_right[trial] = (events[element][0] / 250)/10
    return dict_trials_start, dict_trials_stop, dict_trial_left, dict_trial_right

def crop_trials(raw, trials_start, trials_stop):
    """
    # this might be done as a function composition puting the 
    # function about segementing boundaries inside this function
    Input: 
    Function:
    Output:
    """
    import mne
    croped_file_trials = {}
    for number_trials in range(1, len(trials_start) +1):
        if len(trials_start) > 1:
            #print(number_trials)
            #print(trials_start[number_trials])
            croped_file_trials[number_trials] = raw.copy().crop(tmin=trials_start[number_trials], tmax=trials_stop[number_trials])
            # print(croped_file_trials[i].plot())
            #print(number_trials)
            #print(trials_start[number_trials-1], trials_stop[number_trials-1])
            # print(len(croped_file_trials[i]))
            # print(croped_file_trials[i].info)
            # print(croped_file_trials[i].plot_psd())
        else: 
            continue
            #croped_file_trials[1] = raw
    return croped_file_trials

def crop_MI_rest(raws):
    failed_files = []
    croped_file_MI = {}
    croped_file_rest = {} 
    trial_files = raws.keys()
    for trial in trial_files:
        trial_MI = str(trial) + "_MI"
        trial_rest = str(trial) + "_rest"
        try:
            croped_file_MI[trial_MI] = raws[trial].copy().crop(tmin = 4.25, tmax = 7.25)
            croped_file_rest[trial_rest] = raws[trial].copy().crop(tmin = 0, tmax = 2.5)
        except:
            failed_files.append([trial_MI, trial_rest])
            continue
        """ other options are from 3, 8; 3, 7; 4.25, 7.25 """
        # check if I am alowed to do that and try toi change it 
    return croped_file_MI, croped_file_rest, failed_files

def crop_MI_rest_mat(raws):
    failed_files = []
    croped_file_MI = {}
    croped_file_rest = {} 
    trial_files = raws.keys()
    for trial in trial_files:
        trial_MI = str(trial) + "_MI"
        trial_rest = str(trial) + "_rest"
        try:
            croped_file_MI[trial_MI] = raws[trial].copy().crop(tmin = 2, tmax = 5)
            croped_file_rest[trial_rest] = raws[trial].copy().crop(tmin = 0, tmax = 2)
        except:
            failed_files.append([trial_MI, trial_rest])
            continue
    return croped_file_MI, croped_file_rest, failed_files

def crop_multiple(key_dict, events, data ):
    '''
    Input: dictionary with the name of the file and the data you
            want to segement into trials
    Function: create a loop to call crop_trials 
    Output: dictionary, 
    '''
    boundaries_crop = {}
    croped_files = {}
    #key_dict = dictionary.keys()
    for file in key_dict:
        trials_start, trials_stop, trials_left, trials_right = segmentation_boundaries_gdf(events[file][0])
        croped_file_trials = crop_trials(data[file], trials_start, trials_stop)
        boundaries_crop[file] = [trials_start, trials_stop, trials_left, trials_right]
        croped_files[file] = croped_file_trials
    return boundaries_crop, croped_files


def segmentation_boundaries_csv(annotation_pcf, element):
    import mne 
    dict_trials_start = {}
    dict_trials_stop = {}
    dict_trials_left = {}
    dict_trials_right = {}

    # three zeros => timeStap, 
    # first one, rest zeros => trials 
    # first two, rest zeros => class
    TimeStamp = annotation_pcf[element][0][0][0]
    TimeAxis = annotation_pcf[element][0][1]
    Trials = annotation_pcf[element][1][0][0]
    Class = annotation_pcf[element][2][0][0]
        
    trials_start = []
    trials_stop = []
    trials_left = []
    trials_right = []
        
    for (onset, timestamp, trials, MI_class) in zip(TimeAxis, TimeStamp, Trials, Class):
        if timestamp == -2.996 or timestamp == 0 or timestamp == 5:
            if timestamp == -2.996:
                dict_trials_start[int(trials)] = onset
                #trials_start.append(onset)
            elif timestamp == 0:
                if MI_class == -1:
                    dict_trials_left[int(trials)] = onset
                    #trials_left.append(onset)
                elif MI_class == 1:
                    dict_trials_right[int(trials)] = onset
                    #trials_right.append(onset)
            elif timestamp == 5:
                dict_trials_stop[int(trials)] = onset
                #trials_stop.append(onset)
        """        
        dict_trials_start[int(trials)] = trials_start
        dict_trials_stop[int(trials)] = trials_stop
        dict_trials_left[int(trials)] = trials_left
        dict_trials_right[int(trials)] = trials_right
        """
    return dict_trials_start, dict_trials_stop, dict_trials_left, dict_trials_right
    


def crop_multiple_csv(key_dict, events, data ):
    '''
    Input: dictionary with the name of the file and the data you
            want to segement into trials
    Function: create a loop to call crop_trials 
    Output: dictionary, 
    '''
    boundaries_crop = {}
    croped_files = {}
    #key_dict = dictionary.keys()
    for file in key_dict:
        trials_start, trials_stop, trials_left, trials_right = segmentation_boundaries_csv(events, file)
        croped_file_trials = crop_trials(data[file], trials_start, trials_stop)
        boundaries_crop[file] = [trials_start, trials_stop, trials_left, trials_right]
        croped_files[file] = croped_file_trials
        
    return boundaries_crop, croped_files

def crop_trials_mat(raw, trials_start, trials_stop):
    """
    # this might be done as a function composition puting the 
    # function about segementing boundaries inside this function
    Input: 
    Function:
    Output:
    """
    import mne
    croped_file_trials = {}
    for number_trials in range(len(trials_start)):
        if len(trials_start) > 1:
            #print(number_trials)
            #print(trials_start[number_trials])
            tmin = ((trials_start[number_trials]/512)-2)
            if tmin < 0:
                tmin = 0
            croped_file_trials[number_trials+1] = raw.copy().crop(tmin=tmin, tmax=((trials_stop[number_trials]/512)-2))
            # print(croped_file_trials[i].plot())
            #print(number_trials)
            #print(trials_start[number_trials-1], trials_stop[number_trials-1])
            # print(len(croped_file_trials[i]))
            # print(croped_file_trials[i].info)
            # print(croped_file_trials[i].plot_psd())
            #print(trials_start[number_trials])
        else: 
            continue
    return croped_file_trials

def crop_multiple_mat(events_dict, data): #, events_task):
    '''
    Input: dictionary with the name of the file and the data you
            want to segement into trials
    Function: create a loop to call crop_trials 
    Output: dictionary, 
    '''
    boundaries_crop = {}
    croped_files = {}
    events_list = {}

    for subject in events_dict.keys():
        events_list[subject] = [i for i, e in enumerate(events_dict[subject]) if e == 1]
        croped_file_trials = crop_trials_mat(data[subject], events_list[subject][:-1], events_list[subject][1:])
        # trials need to be devided by 512 to get to seconds such that the resampling has no influence on the secgmenting 
        #boundaries_crop[subject] = [events_list[subject][:-1], events_list[subject][1:], events_task[subject][0], events_task[subject][1]]
        croped_files[subject] = croped_file_trials
        #print(events_list[subject])
    return croped_files # boundaries_crop, 