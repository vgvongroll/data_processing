#%%
''' SEGMENT INTO (MI AND REST) '''
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_saved_trials = multiple_subects_pd(path_file_trials())

#%%
# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = inria_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(inria_saved_trials[run])
    run_dict[run] = loaded_tials

#%%
# segment all trials into MI and rest 
key_runs = run_dict.keys()
run_MI_rest = {}
for run in key_runs:
    from segmentation import crop_MI_rest
    trial_MI, trial_rest = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]
#%%
# check if the naming of MI and rest trials have same side MI-task
print(run_MI_rest['trials_A10_R1_acquisition'][0])
print(run_MI_rest['trials_A10_R1_acquisition'][1])
#%%
#print(run_MI_rest.keys())
keys_MI = run_MI_rest['trials_A10_R1_acquisition'][0].keys()
keys_rest = run_MI_rest['trials_A10_R1_acquisition'][1].keys()
print(keys_MI)
print(keys_rest)

#%%
"""
Calculate the PSD with FFT 
following the code from a notebook 
Lest check if it works 
"""
''' GET THE PSD VALUES AND AVERAGE THEM ACROSS RUNS PER SUBJECT '''
#%%
''' Loop to access individual data '''
from PSD_functions import PSD_values

PSD_per_run = {}
for run in run_MI_rest.keys():
    # devided in runs and participants 
    for trial in range(len(run_MI_rest[run])):
        # pass this information to the funstion 
        # let it calculate all the PSD funtions for each trial 
        # get the values back in one dictionary 
        # get the averaged values between the trials
        # plot those values 
        print(run)
        print(trial)
        print(run_MI_rest[run][trial].keys())
        print(run_MI_rest[run][trial])
        """
        Periodogram_scipy_fft, Spectrum_fourier_transform, Welch_periodogram = PSD_values((run_MI_rest[run][trial]))
        run_trial = str(run) + str(trial)
        PSD_per_run[run_trial] = [Periodogram_scipy_fft, Spectrum_fourier_transform, Welch_periodogram]
        """
#%%
#print(PSD_per_run.keys())
# 'trials_A10_R1_acquisition1'
print(len(PSD_per_run['trials_A10_R1_acquisition0'][0]['10_left_MI'][0]))
print(PSD_per_run['trials_A10_R1_acquisition0'][0]['10_left_MI'][1])

#%%
# create a dataframe that has all the information needed from a subject,
# such that I can average the columns out and use those values to plot the PSD
 
#%%
# save a created figure 
data = run_MI_rest['trials_A10_R1_acquisition'][0]['10_left_MI'][0][0]

from PSD_functions import subplot_PSD
fig1 = subplot_PSD(data)

#%%
''' Loop to access individual data '''
from PSD_functions import subplot_PSD
from os import path
outpath = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PSD_plots"

for run in run_MI_rest.keys():
    for trial in range(len(run_MI_rest[run])):
        for trial_data in run_MI_rest[run][trial].keys():
            name_plot = str(run) + str(trial) + str(trial_data) + ".pdf"
            fig = subplot_PSD(run_MI_rest[run][trial][trial_data][0][0])
            fig.savefig(path.join(outpath, name_plot))
#%%
"""
for later to calculate the 
Mu suppression values 
"""
# %%
from welch_psd import average_over_band, classify_trials
# i will probably not need this functions. They should be changed or deleted
