
# use mne functions on raw data 
#   inria 
import matplotlib.pyplot as plt
import mne

'''
plt.pause(10)
resampled_A9 = inria["A9_R5_onlineT"].resample(250)
print(inria["A9_R5_onlineT"].plot())
print(resampled_A9.plot())
print(resampled_A9.info)
resampled_A9.plot_psd()
inria["A9_R5_onlineT"].plot_psd()
plt.pause(10)
'''

def resample_250(data):
    '''
    Input:  data in mne_raw_data format, the individual files of data
            are  loaded
    Function:
    Output: resampled data stored 

    '''
    data_resampled = {}
    df_data = data.keys()
    for file in df_data:
        data_resampled[file] = data[file].resample(250)
    return data_resampled

'''
hi = resample_250(inria)
print(hi[5].plot())
print(hi[5].info)
print(hi[5])
'''
def channels(data):
    '''
    Input:
    Function:
    Output:
    '''
    data_channels = {}
    df_data = data.keys()
    for file in df_data:
        data_channels[file] = data[file].pick_channels(["C3", "C4"])
    return data_channels

def filter_bandpass(data):
    '''
    Input:  data in mne_raw format, every individual file is loaded 
    Function:   for every file the name a=of the file is saved as a key 
                for the dictionary, while only the selected channels are 
                safed into the dictionary.  
    Output: Dictionary (only selected channels (C3, C4))
    '''
    data_filtered = {}
    df_data = data.keys()
    for file in df_data:
        data_filtered[file] = data[file].filter(l_freq = 8, h_freq = 13, picks = ["C3","C4"])
    return data_filtered