# pip install mnelab
# This code is going to combine all different functions needed to process the data 

# I decided to load all the different types of file and convert them into 
# mne.io.raw, then I will pre-process it 
# calculate the mu suppression indices and once the individual values are out
# I will combine all of them into a numpy array (subject number, dataset, gender, 
# class, mu suppresion, overall mu supression)

#%%
'''
LOAD GDF FILES (INRIA)
GET PATH 
CONVERT INTO RAW MNE 
'''
# %%
# load GDF files, 
#   get the file path and load all files as raw mne 
from load_gdf_files import load_raw_mne, path_file_Inria
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria = load_raw_mne(path_file_Inria())

# %%
inria_df = inria.keys()
print(inria_df)
print(inria["A9_R6_onlineT"].info)
#%%
print(inria["A9_R6_onlineT"])
inria["A9_R6_onlineT"].plot()

#%%
# use mne functions on raw data 
#   inria 
import matplotlib.pyplot as plt
 
plt.pause(10)

resampled_A9 = inria["A9_R5_onlineT"].resample(250)
#print(inria["A9_R5_onlineT"].plot())
print(resampled_A9.plot())
#print(resampled_A9.info)
resampled_A9.plot_psd()
#inria["A9_R5_onlineT"].plot_psd()
plt.pause(10)

#%%
a = resampled_A9.pick_channels(["C3", "C4"])
a.plot()
a.plot_psd()

#%% 
'''
LOAD CSV FILES
GET FILE PATH 
PCF
'''
#%%
from load_csv_files import load_csv, path_file_PCF
pcf = load_csv(path_file_PCF())
pcf_df = pcf.keys()
print(pcf_df)
print(len(pcf_df))
print(pcf["Subject7_eeg_4"])

#%%
# load csv files as raw mne
    # PCF
from mne_convertion import convert_csv_mne
import matplotlib.pyplot as plt
# plt.pause() lest the plot window stay open for the time specified in the brackets 

# get basic info from raw_csv
raw_pcf = convert_csv_mne(pcf)
print(raw_pcf['Subject9_eeg_4'].plot())
print(raw_pcf['Subject9_eeg_4'].info)
plt.pause(10)

#%%
'''
LOAD MATLAB FILES 
GET FILES PATH 
EMB = EEG_MI_BCI
'''
#%%
from load_mat_python import path_file_mat
path_m = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\eeg_MI_BCI"
emb_p = path_file_mat(path_m)
print(emb_p)

# %%
# load matfiles and access nested arrays 
from load_mat_python import loadmat, _check_keys, _todict
from pprint import pprint
# for file in a:
emb = _check_keys(loadmat(emb_p[1]))
emb_df = emb['eeg'].keys()
print(emb_df)
# %%
for i in range(len(emb['eeg']['imagery_right'])):
    pprint(emb['eeg']['imagery_right'][i])
print(emb['eeg']["imagery_right"][4][1])

# %%
from mne_convertion import convert_emb_mne
a, b = convert_emb_mne(emb_p)
print(a)
# %%
print(b[1])

# %%
from load_mat_python import loadmat, _check_keys, _todict
c = _check_keys(loadmat(emb_p[3]))
print(c["eeg"].keys())
print(c['eeg']['movement_left'])

#%%
'''
PRE-PROCESS THE DIFFERENT FIELS 
RESAMPLING 
FILTER
SELECT CHANELLS 
'''
# %%
from resampling_bandpass import resample_250
hi = resample_250(inria)
# %%
a = hi.keys()
h = []
for element in a:
    h.append(element)
#%%
print(hi[h[5]].plot())
print(hi[h[5]].info)

#%%
from resampling_bandpass import filter_bandpass
my = filter_bandpass(hi)
# %%
from resampling_bandpass import channels
nu = channels(my)
#%%
print(my[h[5]].plot())
print(my[h[5]].info)
print(nu[h[5]].plot())
print(nu[h[5]].info)
# %%
'''
SAVE THE PROCESSED DATA INTO FILES
'''
#%%
from save_processed_data import save_processed_files
j = save_processed_files(nu)
# %%
print(j)

#%%
# resampled_A9 = inria["A9_R5_onlineT"].resample(250)
#raw = resampled_A9
