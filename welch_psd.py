
import mne 
import numpy as np

def welch_psd_values(trial):
    '''
    Input:  dictionary, with the trail number and the eeg signals
    Function:   create a dictionary with the welch_psd values
    Output: dictionary 
    '''
    key_trials = trial.keys()
    psds = {}
    for element in key_trials:
        psds[element], freq = mne.time_frequency.psd_welch(trial[element], fmin = 0, fmax = 20, n_fft = 250)
    return psds, freq

def average_over_band(trial):
    '''
    Input:
    Function:   get the psd values per channel averaged 
                over the frequencey band
    Output: dictionary with a nested list [0]= C3, [1]=C4
    '''
    keys_trial = trial.keys()
    averaged_psd = {}
    for element in keys_trial:
        averaged_psd[element] = [np.average(trial[element][0]), np.average(trial[element][1])]
    return averaged_psd

def classify_trials(trial):
    '''
    Input:
    Function: Classify trials into left and right
    Output:
    '''
    key_trials = trial.keys()
    psds_right = {}
    psds_left = {}
    for element in key_trials:
        if "_right_" in element:
            psds_right[element] = trial[element]
        elif "_left_" in element:
            psds_left[element] = trial[element]
    return psds_left, psds_right

def average_per_subject(trials):
    '''
    Input:
    Function: go through all the individual trial and get 
                two different averages, one per task (right,
                left)
    Output:
    '''
    key_trials = trials.keys()
    C3 = []
    C4 = []
    for element in key_trials:
        C3.append(trials[element][0])
        C4.append(trials[element][1])
    channel_subject_average = [np.average(C3), np.average(C4)]
    return channel_subject_average
