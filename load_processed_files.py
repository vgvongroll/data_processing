import mne
import os

# Getting file path 
# Use path to load files

def path_file_processed_data(path_pd):
    '''
    Input:  add the path to saved processed files (can be added)
    Function:   creates a list of paths for every processed and saved file 
                (has an extra external loop to enter individual subjects folder 
                inside the given path) Files contain EEG signal
    Output:     retuns a list with the paths to all individual files
    '''
    # path_pd = r'C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria' 
    #"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files"
    pd_folder = os.listdir(path_pd)
    pd_file = []
    for file in pd_folder:
        path = path_pd + "\\" + file
        pd_file.append(path)

    '''
    pd_folder = os.listdir(path_pd) # list the files that are at that directory
    # print(pd_folder)
    pd_file = []
    pd_files = []
    for folder in pd_folder:
        pd_file = path_pd + "\\" + folder
        # print(Inria_file)
        pd_file_dir = os.listdir(pd_file)
    
        for file in pd_file_dir:
            if "fif" in file:
                path = pd_file + "\\" + file
                pd_files.append(path)
    '''
    return pd_file

def path_file_trials(path_pd = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_croped"):
    """
    Input:  
    Function:   creates a list of paths fro every trial 
    Output: retuns a list with the paths to all individual 
            trial files
    """
    #path_pd = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_croped"
    #r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\30_files_trials"
    #r'C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_trials' 
    #r'C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\Inria_trials_no_resample'
    pd_folder = os.listdir(path_pd) # list the files that are at that directory
    # print(pd_folder)
    pd_file = []
    pd_files = []
    for folder in pd_folder:
        pd_file = path_pd + "\\" + folder
        # print(Inria_file)
        pd_file_dir = os.listdir(pd_file)
    
        for file in pd_file_dir:
            if "fif" in file:
                path = pd_file + "\\" + file
                pd_files.append(path)

    return pd_files
    
# pprint(path_file_processed_data())
# print(len(path_file_processed_data()))

def load_processed_mne(path_list):
    '''
    Input:  A list with paths to individual gdf files
    Function:   save the loaded files in a dictionary 
                (key is the name of the file (without file extention),
                item is the loaded raw_mne file)
    Output:     Dictionary with file names and loaded raw_mne file
    '''
    raw_dic = {}
    for path in path_list:
        name = os.path.basename(path)
        raw_dic[name[:-4]] = mne.io.read_raw_fif(path, allow_maxshield=False, preload=False, on_split_missing='raise', verbose=None)
    return raw_dic

def multiple_subects_pd(gen_path_list):
    processed_dict = {}
    trials_list = []
    previous_name = ''
    for gen_path in gen_path_list:
        gen_name = os.path.dirname(gen_path)
        name = os.path.basename(gen_name)
        if previous_name == name:
            trials_list.append(gen_path)
        elif previous_name == '':
            previous_name = name
            trials_list.append(gen_path)
        else:
            processed_dict[previous_name] = trials_list
            previous_name = name
            trials_list = []
    return  processed_dict