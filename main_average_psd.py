#%%
''' SEGMENT INTO (MI AND REST) '''
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
inria_saved_trials = multiple_subects_pd(path_file_trials())

#%%
# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = inria_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(inria_saved_trials[run])
    run_dict[run] = loaded_tials

#%%
# segment all trials into MI and rest 
key_runs = run_dict.keys()
run_MI_rest = {}
for run in key_runs:
    from segmentation import crop_MI_rest
    trial_MI, trial_rest = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]


#%%
"""
FIND A WAY TO GET ALL THE PSD VALUES 
AND AVERAGE THEM PER 
    RUN
    SUBJECT 
HAVING 8 DIFFERENT VALUES 
    REST VS MI
    C3 VS C4
    RIGHT VS LEFT
"""
#%%
''' Loop to access individual data '''
from average_PSD import PSD_periodogram_fft, average_per_run

PSD_per_subject = {}
PSD_run_avg = []
PSD_run_avg1 = []
for run in run_MI_rest.keys():
    #devided in runs and participants 
    #for trial in range(len(run_MI_rest[run])):
        # pass this information to the funstion 
        # let it calculate all the PSD funtions for each trial 
        # get the values back in one dictionary 
        # get the averaged values between the trials
        # plot those values 
        
    df, _ = PSD_periodogram_fft(run_MI_rest[run][0])
    _, df1 = PSD_periodogram_fft(run_MI_rest[run][1])
    '''
        avg_df = average_per_run(df)
            #avg_df1 = average_per_run(df1)
            #print(avg_df)
        PSD_run_avg.append(avg_df)
            #PSD_run_avg1.append(df1)
        PSD_per_subject[run] = (PSD_run_avg) #, PSD_run_avg1)
        '''
"""
frames = [df1, df2, df3]
result = pd.concat(frames)
"""
#%%
df
#%%
df1
#%%
#print(PSD_per_subject.keys())
for i in PSD_per_subject.keys():
    print(i)
    PSD_per_subject[i][0]
#%%
PSD_per_subject["trials_A10_R1_acquisition"][0][0]
print(PSD_run_avg)
print(PSD_run_avg1)
print(run_MI_rest.keys())
# %%
from average_PSD import PSD_periodogram_fft
data = run_MI_rest["trials_A10_R1_acquisition"][1]
df, df1 = PSD_periodogram_fft(data)
print(len(df))
df1
#%%
df
#%%
from average_PSD import average_per_run
avg_df = average_per_run(df1)
#%%
avg_df[1]

#%%from average_PSD import PSD_periodogram_fft
data = run_MI_rest["trials_A10_R1_acquisition"][0]
df, df1 = PSD_periodogram_fft(data)
print(len(df))
df
#%%
df
#%%
from average_PSD import average_per_run
avg_df1 = average_per_run(df1)
#%%
avg_df1[1]

#%%
'''
import matplotlib.pyplot as plt
plt.plot(df_left_C3_freq, df_left_C3_PSD)
plt.xlim([0, 20])
'''
#%%
a = avg_df1[1][avg_df1[1]["group_name"].str.contains("left_C4", case=False)]
a

#%%
df_sub = df.iloc[:4]
a = df_sub.iloc[:, 3:]
a
#%%
C3_one = list(a.iloc[0])
print(C3_one)
# %%
# Create an empty list
Row_list =[]
  
# Iterate over each row
for i in range(4):    
    my_list = list(a.iloc[i])   
    # append the list to the final list
    Row_list.append(my_list)
  
#%%
# Print the list
print(len(Row_list))
# %%
import matplotlib.pyplot as plt
plt.plot( Row_list[0], Row_list[1],)
#plt.xlim([0, 20])

#%%
import matplotlib.pyplot as plt
plt.plot(Row_list[2], Row_list[3])
plt.xlim([0, 20])
# %%
import numpy as np
print(np.average(Row_list[2]))
print(np.average(Row_list[0]))
print(np.average(Row_list[3]))
print(np.average(Row_list[1]))
# %%
df
# %%
print(len(Row_list[3]))
print(376/125)

# %%
