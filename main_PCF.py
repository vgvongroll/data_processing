
#%%
from load_csv_files import load_csv, path_file_PCF
from mne_convertion import convert_csv_mne
import matplotlib.pyplot as plt

import mne

#%% 
"""
THIS FILE IS THE SCRIPT USED TO PERFORM THE WHOLE DATA ANALYSIS FROM 
THE PSYCHOLOGICAL COGNITIVE FUNCTIONS
IT FOLLOWS THE WHOLE PIPELINE
"""

#%%
''' Load the files '''
# Get the path from the PCF files  
# load the csv files and convert them into Raw MNE objects
from load_csv_files import load_csv, path_file_PCF
path_PCF = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\psychological_cognitive_factors\Raw eeg 2"
pcf = load_csv(path_file_PCF(path_PCF))

#%%
''' Convert csv into Raw MNE '''
from mne_convertion import convert_csv_mne
import matplotlib.pyplot as plt
# get basic info from raw_csv
raw_pcf = convert_csv_mne(pcf)

#%%
'''
# plt.pause() lest the plot window stay open for the time specified 
# in the brackets 
print(raw_pcf)
print(raw_pcf['Subject9_eeg_4'].plot())
print(raw_pcf['Subject9_eeg_4'].info)
plt.pause(10)
pcf_df = pcf.keys()
print(len(pcf_df))
#%%
print(raw_pcf["Subject9_eeg_4"][0][1])
'''
#%%
''' Devide dataset in two, for the laptop to not crash '''
key_pcf = list(raw_pcf.keys())
key_pcf1 = key_pcf[:55]
key_pcf2 = key_pcf[55:110]
key_pcf3 = key_pcf[110:165]
key_pcf4 = key_pcf[165:]


#%%
pcf1 = {}
for _ in key_pcf4:
    pcf1[_] = raw_pcf[_]

#%%
''' Create the annotations from the rows "timestamp", "trial", "class" '''
from events_gdf import load_events, channels_annotation
#raw_pcf = load_events()
events_pcf = channels_annotation(pcf1)
#%%
""" INSPECT THE INDIVIDUAL TRIALS """
''' Figure out wether to reject some trials '''
# The code for it is still missging, It needs to reject the trials that 
# have a higher value than 100 mV
""" APPLY A SPATIAL FILTER """
# Apply a spatial filter in oerder to have cleaner data
# the code is still not there
# first it is immportant to compare the whether applying the filter 
# improves the MSI values

#%%
""" PRE-PROCESSED THE INDIVIDUAL FILES """
''' Resampling, fitering, selecting channels '''
# Adjust the folder from the files A, B, C
from resampling_bandpass import channels
ch_pcf = channels(pcf1)

#%%
from resampling_bandpass import filter_bandpass
filtered_pcf = filter_bandpass(pcf1)

#%%
from resampling_bandpass import resample_250
resampled_pcf = resample_250(filtered_pcf)
key_resampled = resampled_pcf.keys()
print(len(key_resampled))

#%%
""" CROP THE DATA INTO TRIALS """
from segmentation import crop_multiple_csv
boundaries_crop, croped_files = crop_multiple_csv(key_resampled, events_pcf, resampled_pcf)
#print(croped_files)

#%%
''' Save the the devided files (into individual trials) in folder'''
from save_processed_data import save_trials
save_trials(croped_files, boundaries_crop)





#%%
""" CALCULATE THE MU-SUPPRESSION VALUES """
''' Load the individual trials '''
from load_processed_files import load_processed_mne, path_file_trials, multiple_subects_pd
# try accesing one of the files and ploting the EEG signals based on the functions created 
path_pd = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PCF_croped"
pcf_saved_trials = multiple_subects_pd(path_file_trials(path_pd))

# create a nested dictionary with run (key1), trial(key2), loaded (value)
key_saved_trials = pcf_saved_trials.keys()
run_dict = {}
for run in key_saved_trials:
    loaded_tials = load_processed_mne(pcf_saved_trials[run])
    run_dict[run] = loaded_tials

#%%
''' Segment trial into MI and Rest '''
from segmentation import crop_MI_rest
key_runs = run_dict.keys()
run_MI_rest = {}
failed_run = {}
for run in key_runs:
    trial_MI, trial_rest, failed_files = crop_MI_rest(run_dict[run])
    run_MI_rest[run] = [trial_MI, trial_rest]
    failed_run[run] = failed_files
# apparently the trial C86 presented some problems, 
# specifically runs: R3_onlineT, R4_onlineT, R5_onlineT, R6_onlineT

#%%
for i in failed_run.keys():
    if len(failed_run[i]) > 0:
        print(i)
        print(failed_run[i])

#%%
''' Create pandas dataframe with individual MSI per trial '''
from mu_suppression import get_PSD_values
df = get_PSD_values(run_MI_rest=run_MI_rest, dataset="PCF")
df

#%%
''' Crate pandas dataframe with averaged MSI for each run '''
from mu_suppression import MSI_per_run
df_run = MSI_per_run(df)
df_run

#%%
''' Create pandas dataframe with averaged MSI for each subject'''
from mu_suppression import MSI_per_subject
df_avg = MSI_per_subject(df)
df_avg

#%%
# Save the DF into CSV files in order to access them later 
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PCF_MSI\PCF_PSD.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PCF_MSI\PCF_MSI_run.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_run.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PCF_MSI\PCF_MSI_avg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_avg.to_csv(filepath)

#%%
""" TO PLOT PSD VALUES FOR ROWS IN DATAFRAME """
neg = df[df["MSI"] < 0]
neg
#%%
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\PCF_MSI\PCF_MSI_neg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
neg.to_csv(filepath)
#%%
from negative_MSI import PSD_run
PSD_per_run = PSD_run(run_MI_rest)

#%%
from negative_MSI import plots_negative_trials
fig_list = plots_negative_trials(df, PSD_per_run)
