
import mne
import numpy as np
import os

def convert_csv_mne(csv_file):
    '''
    Input:  dictionary with the loaded csv files
    Function:   create mne.info for the loaded data and 
                create a dictionary with the new raw_mne 
                formated files
    '''
    # info is from the PCF = Psychological and Cognitive Factors in Motor Imagery Brain Computer Interfaces dataset
    ch_name = ["TimeStamp", "F3", "Fz", "F4", "FC5", "FC1", "FC2", "FC6", "T7", "C3", "C4", "Cz", "T8", "CP5", "CP1", "Cp2", "CP6", "trial", "class"]
    sfre = 250
    info = mne.create_info(ch_names = ch_name, sfreq = sfre)
    
    raw_dic = {}
    for element in csv_file:
        T_element = csv_file[element].T
        raw_dic[element] = mne.io.RawArray(T_element, info)
    return raw_dic

def create_info(loaded_file):
    ''' Create a Raw info object '''
    # To initialize a minimal Info object requires a list of channel names, 
    # and the sampling frequency.
    ch_names = ["FP1", "AF7", "AF3", "F1", "F3", "F5", "F7", "FT7", 
               "FC5", "FC3", "FC1", "C1", "C3", "C5", "T7", "TP7", 
               "CP5", "CP3", "CP1", "P1", "P3", "P5", "P7", "P9", 
               "PO7", "PO3", "O1", "Iz" , "Oz", "POz", "Pz", "CPz",
               "FPz", "FP2", "AF8", "AF4", "AFz", "Fz", "F2", "F4", 
               "F6", "F8", "FT8", "FC6", "FC4", "FC2", "FCz", "Cz", 
               "C2", "C4", "C6", "T8", "TP8", "CP6", "CP4", "CP2", 
               "P2", "P4", "P6", "P8", "P10", "PO8", "PO4", "O2", 
               "EMG1", "EMG2", "EMG3", "EMG4"]
    ch_types = ['eeg'] * 64 + ['emg'] *4
    sampling_freq = loaded_file['eeg']['srate'] # 512 in Hertz
    info = mne.create_info(ch_names, ch_types=ch_types, sfreq=sampling_freq)
    #info['bad_voltage'] = loaded_file['eeg']['bad_trial_indices']['bad_trial_idx_voltage'][0]
    #info['bads'] = list(loaded_file['eeg']['bad_trial_indices']['bad_trial_idx_mi'][0])
    return info

def convert_emb_mne(loaded_data):
    '''
    Input:
    Function:
    Output:
    '''
    mne_dict = {}
    raw_rest_d = {}
    raw_left_d = {}
    raw_right_d = {}
    events_dict = {}
    for file in loaded_data.keys():
        info = create_info(loaded_data[file])
        rest = loaded_data[file]['eeg']['rest']
        left_imagery = loaded_data[file]['eeg']['imagery_left']
        right_imagery = loaded_data[file]['eeg']['imagery_right']
        events_dict[file] = loaded_data[file]['eeg']['imagery_event']

        raw_rest = mne.io.RawArray(rest, info)
        raw_rest_d[file] = raw_rest
        raw_left_imagery = mne.io.RawArray(left_imagery, info)
        raw_left_d[file] = raw_left_imagery
        raw_right_imagery =mne.io.RawArray(right_imagery, info)
        raw_right_d[file] = raw_right_imagery

    mne_dict['rest'] = raw_rest_d
    mne_dict['left'] = raw_left_d
    mne_dict['right'] = raw_right_d    
    return mne_dict, events_dict
