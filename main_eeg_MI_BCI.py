
#%%
"""
THIS FILE IS THE SCRIPT USED TO PERFORM THE WHOLE DATA ANALYISIS FROM THE 
EEG_MI_BCI DATASET
EMB = EEG_MI_BCI
IT FOLLOWS THE WHOLE PIPEPLINE 
"""

#%%
''' Load matlab files '''
# get the files path 
from load_mat_python import path_file_mat, load_matlab_data, loadmat
from pprint import pprint

path_m = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Datasets\eeg_MI_BCI"
emb_p = path_file_mat(path_m)

#%%
# loading data version 1
loaded_data = load_matlab_data(emb_p)
#%%
events_dict = {}
for file in loaded_data.keys():
    events_dict[file] = loaded_data[file]['eeg']['imagery_event']
# %%
'''
# loading data version 2
from mne_convertion import convert_emb_mne
loaded_data, subjects = convert_emb_mne(emb_p)
print(loaded_data)
print(subjects)
'''
#%%

# only use for further analysis such that it goes faster 
# load matfiles and access nested arrays 
import os
loaded_data = {}
for file in range(5):
    name = os.path.basename(emb_p[file])
    loaded_data[name[:-4]] = loadmat(emb_p[file])

#%%
emb_df = loaded_data['s01']['eeg'].keys()
print(emb_df)






#%%
"""
THE MATLAB DATA FORMAT IS REALLY BAD, I NEED TO CREATE RAW MNE OBJECTS THAT 
I CAN USE. 
IN ORDER TO DO SO, THE FOLLOWING CODE NEEDS TO BE RUNNED
"""
#%%
''' Create a Raw object '''
''' Convert matlab into Ra MNE'''
from mne_convertion import convert_emb_mne
raw_dic, events_dict = convert_emb_mne(loaded_data)

#%%
""" INSPECT THE INDIVIDUAL TRIALS """
''' Figure out wether to reject some trials '''
# The code for it is still missging, It needs to reject the trials that 
# have a higher value than 100 mV
""" APPLY A SPATIAL FILTER """
# Apply a spatial filter in oerder to have cleaner data
# the code is still not there
# first it is immportant to compare the whether applying the filter 
# improves the MSI values

#%%
""" PRE-PROCESSED THE INDIVIDUAL FILES """
''' Resampling, fitering, selecting channels '''
# Adjust the folder from the files A, B, C
from resampling_bandpass import channels
ch_eeg_list = []
for i in raw_dic.keys():
    ch_eeg = channels(raw_dic[i])
    ch_eeg_list.append(ch_eeg)

#%%
from resampling_bandpass import filter_bandpass
filtered_eeg_list = []
for i in range(len(ch_eeg_list)):
    filtered_eeg = filter_bandpass(ch_eeg_list[i])
    filtered_eeg_list.append(filtered_eeg)

#%%
from resampling_bandpass import resample_250
resampled_eeg_list = []
for i in range(len(filtered_eeg_list)):
    resampled_eeg = resample_250(filtered_eeg_list[i])
    resampled_eeg_list.append(resampled_eeg)
#%%
for i in range(len(resampled_eeg_list)):
    key_resampled = resampled_eeg_list[i].keys()
    print(len(key_resampled))

#%%
''' Save the processed data into files '''
# allowes to do the previous steps only onve for each folder
from save_processed_data import save_processed_files
#names_sppd = save_processed_files(resampled_eeg_list[1])
names_sppd = save_processed_files(resampled_eeg_list[0])







#%%
""" LOAD PRE-PROCESSED DATA """
from load_processed_files import load_processed_mne, path_file_processed_data
# Check whether to do this separately for each folder or as one dataset 
# As one data set would be best   
path_pd_l = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_dataset\left" 

eeg_processed_l = load_processed_mne(path_file_processed_data(path_pd_l))
key_eeg_p = list(eeg_processed_l.keys())

path_pd_r = r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_dataset\right"
eeg_processed_r = load_processed_mne(path_file_processed_data(path_pd_r))
#%%
''' Segment into individual trials per subject '''
''' segment into trials '''
# it might be necessary to use the files from the sequenced, but I still 
# do not really uinderstand how to used them 
# check at the bottom of the page for the code 
from segmentation import crop_MI_rest_mat, crop_multiple_mat
crop_files_l = crop_multiple_mat(events_dict, eeg_processed_l) #, events_task)
crop_files_r = crop_multiple_mat(events_dict, eeg_processed_r)
#%%
#print(crop_files)
crop_files_l['s01'][1].plot(scalings = dict(eeg=1e+03))





#%%
''' Segment trial into MI and Rest '''
from segmentation import crop_MI_rest_mat
key_subs = crop_files_l.keys()
run_MI_rest_l = {}
failed_run = {}
for sub in key_subs:
    trial_MI, trial_rest, failed_files = crop_MI_rest_mat(crop_files_l[sub])
    run_MI_rest_l[sub] = [trial_MI, trial_rest]
    failed_run[sub] = failed_files
#%%
key_subs = crop_files_l.keys()
run_MI_rest_r = {}
failed_run = {}
for sub in key_subs:
    trial_MI, trial_rest, failed_files = crop_MI_rest_mat(crop_files_r[sub])
    run_MI_rest_r[sub] = [trial_MI, trial_rest]
    failed_run[sub] = failed_files

#%%
#print(run_MI_rest['s01'][0].keys())
run_MI_rest_l['s01'][0]['1_MI'].plot(scalings = dict(eeg=1e+02))
#%%
run_MI_rest_l['s01'][1]['1_rest'].plot(scalings = dict(eeg=1e+03))

#%%
''' Create pandas dataframe with individual MSI per trial '''
from mu_suppression import get_PSD_values_EEG
df_l = get_PSD_values_EEG(run_MI_rest=run_MI_rest_l, task="left")
df_l
#%%
df_r = get_PSD_values_EEG(run_MI_rest=run_MI_rest_r, task="right")
df_r
#%%
# Combine datasets 
import pandas as pd
df_r_c = df_r.reset_index(drop=True)
# Stack the DataFrames on top of each other
df = pd.concat([df_l, df_r_c], axis=0)
df
#%%
''' Create pandas dataframe with averaged MSI for each subject'''
from mu_suppression import MSI_per_subject
df_avg = MSI_per_subject(df)
df_avg


#%%
# Save the DF into CSV files in order to access them later 
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_MSI\PSD_l.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_l.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_MSI\PSD_r.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_r.to_csv(filepath)
#%%
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_MSI\PSD.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df.to_csv(filepath)

filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_MSI\MSI_avg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
df_avg.to_csv(filepath)

#%%
""" TO PLOT PSD VALUES FOR ROWS IN DATAFRAME """
neg = df[df["MSI"] < 0]
neg
#%%
from pathlib import Path  
filepath = Path(r"C:\Users\vgvon\Documents\TilburgUniversity\Sem_6_2022\Thesis\Git\data_processing\Saved_processed_files\EEG_MI_BCI_MSI\MSI_neg.csv")  
filepath.parent.mkdir(parents=True, exist_ok=True)  
neg.to_csv(filepath)
#%%
from negative_MSI import PSD_run
PSD_per_run = PSD_run(run_MI_rest_l)

#%%
from negative_MSI import plots_negative_trials
fig_list = plots_negative_trials(df_l, PSD_per_run)
