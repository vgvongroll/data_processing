
from __future__ import division
import scipy.fft as fft
import matplotlib.pyplot as plt
import numpy as np
import math
import scipy.signal as signal
import mne 

from welch_psd import welch_psd_values



def get_raw_data(run_MI_rest):
    """
    Input: nested dictionary, 
    Function:   loops though all the different netsed iterables to access all
                individual raw data; use as a decoder before other functions
    Output: no output; create variable that is accessible by decorated function
    """
    # Loop over the runs
    for run in run_MI_rest.keys():
        # two  dictionaries 
            # MI tasks [0]
            # rest tasks [1]

        # loop over the MI and rest 
        for trial in range(len(run_MI_rest[run])):
            # print dictionaries with all 40 trials per run
            
            #  Loop over trial_name and access the trial data
            for trial_data in run_MI_rest[run][trial].keys():
                # access the individual raw data 
                individual = run_MI_rest[run][trial][trial_data]
                # At this moment you can plot it 

def access_values_raw(individual, channel):
    if channel == "C3":
        # amplitude values
        individual[0][0]
        # time axis
        individual[0][1]
    elif channel == "C4":
        # amplitude values
        individual[1][0]
        # time axis
        individual[1][1]

def Fourier_transform(individual):
    """
    CODE COPIED FROM THE NOTEBOOK 
    https://notebook.community/JoseGuzman/myIPythonNotebooks/SignalProcessing/Welch's%20periodogram
    """
    sr = 250

    # Fourier transform
    FourierCoeff = np.fft.fft(individual)/individual.size
    DC = [np.abs(FourierCoeff[0])]
    amp = np.concatenate((DC, 2*np.abs(FourierCoeff[1:])))  

    # compute frequencies vector until half the sampling rate
    Nsamples = int( math.floor(individual.size/2) )
    hz = np.linspace(0, sr/2., num = Nsamples + 1 )

    fig = plt.figure(figsize=(8, 6))
    plt.plot(hz, amp[0][:len(hz)]) 
    plt.title("Amplitude spectrum (Fourier transform)")
    plt.ylabel('Amplitude ($\mu V$)')
    plt.xlabel('Frequency (Hz)')
    plt.xlim(0, 20)
    plt.show()
    return fig

def scipy_signal_periodogram(individual):
    # gives exactly the same result as scipy_fft and numpy_fft
    b, c = signal.periodogram(individual, fs = 250)

    fig = plt.figure(figsize=(8, 6))
    plt.plot(b, c[0])
    plt.xlim([0, 20])
    plt.title("Periodogram function scipy")
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()
    return fig

'''
def scipy_fft(individual):
    """
    Input:  
    Function:
    Output: plot the PSD, x-axis = frequency Hz, y-axis = amplitude [V**2/Hz]
    """
    ps = np.abs(fft.fft(individual))**2
    time_step = 1 / 250
    freqs = fft.fftfreq(individual.size, time_step)

    fig, ax = plt.subplots(figsize=(8, 6))
    ax.plot(freqs, ps[0])
    ax.set_title("scipy FFT")
    ax.set_xlim([0, 20])
    plt.show()
    return fig, ax
'''
'''
# exaclty the same result as the scipy_fft
def numpy_fft(individual):
    ps = np.abs(np.fft.fft(individual))**2
    time_step = 1 / 250
    freqs = np.fft.fftfreq(individual.size, time_step)

    fig = plt.figure(figsize=(8, 6))
    plt.plot(freqs, ps[0])
    plt.title("numpy FFT")
    plt.xlim([0, 20])
    plt.show()
    return fig
'''

def mne_welch_psd(run_MI_rest):
    for run in run_MI_rest.keys():
        for trial in range(len(run_MI_rest[run])):
            a = run_MI_rest[run][trial]

    psds_MI, freq = welch_psd_values(a)

    key_psds =psds_MI.keys()
    figs = []
    for key in key_psds:

        fig = plt.figure(figsize=(8, 6))
        plt.plot(freq, psds_MI[key][1])
        plt.title("MNE Welch_PSD")
        plt.xlim(0, 20)
        plt.ylabel('Power spectrum ($\mu V^2$)')
        plt.xlabel('Frequency (Hz)')
        plt.show()

        figs.append(fig)
    return figs

def scipy_signal_welch(individual):

    # Perform Welch's periodogram
    segment = int( 250*2 ) 
    myhann = signal.get_window('hann', segment)
    '''
    # obtain simply Power (amplitude^2) withouth tappering
    myparams = dict(fs = 250, nperseg = segment, window = np.ones(segment), 
                    noverlap = 0, scaling = 'spectrum', return_onesided=True)
    freq, ps = signal.welch(x = individual, **myparams)# units uV**2
    ps = 2*ps # correct for negative frequencies
    '''
    #obtain Power density (amplitude^2/Hz) withouth tappering
    myparams = dict(fs = 250, nperseg = segment, window = np.ones(segment), 
                    noverlap = 0, scaling = 'density', return_onesided=True)
    freq, psd = signal.welch(x = individual, **myparams)# units uV**2/Hz
    psd = 2*psd # correct for negative frequencies
    
    fig = plt.figure(figsize=(8, 6))
    plt.title("Power density (Welch's periodogram)")
    plt.plot(freq, psd[0], color='k', lw=2) 
    plt.xlim(0, 20)
    plt.ylabel('Power spectrum') #($\uV**2/Hz$)')
    plt.xlabel('Frequency (Hz)')
    plt.show()
    return fig

def subplot_PSD(individual):
    fig, axs = plt.subplots(2, 2)
    
    b, c = signal.periodogram(individual, fs = 250)
    axs[0, 0].plot(b, c[0])
    axs[0, 0].set_title("Periodogram scipy (fft)", pad=20)
    axs[0, 0].set_ylabel("PSD [V**2/Hz]")
    axs[0, 0].set_xlabel("frequency Hz")
    axs[0, 0].set_xlim([0, 20])

    # Fourier transform
    FourierCoeff = np.fft.fft(individual)/individual.size
    DC = [np.abs(FourierCoeff[0])]
    amp = np.concatenate((DC, 2*np.abs(FourierCoeff[1:])))  
    # compute frequencies vector until half the sampling rate
    Nsamples = int( math.floor(individual.size/2) )
    hz = np.linspace(0, 250/2., num = Nsamples + 1 )

    axs[0, 1].plot(hz, amp[0][:len(hz)])
    axs[0, 1].set_title("Amplitude spectrum (Fourier transform)", pad = 20)
    axs[0, 1].set_ylabel('Amplitude ($\mu V$)')
    axs[0, 1].set_xlabel("frequency Hz")
    axs[0, 1].set_xlim([0, 20])

    segment = int( 250*2 ) 
    myhann = signal.get_window('hann', segment)
    #obtain Power density (amplitude^2/Hz) withouth tappering
    myparams = dict(fs = 250, nperseg = segment, window = np.ones(segment), 
                    noverlap = 0, scaling = 'density', return_onesided=True)
    freq, psd = signal.welch(x = individual, **myparams)# units uV**2/Hz
    psd = 2*psd # correct for negative frequencies
    
    axs[1, 0].plot(freq, psd[0], color='k', lw=2) 
    axs[1, 0].set_title("Welch's periodogram", pad= 20)
    axs[1, 0].set_ylabel('Power spectrum  [V**2/Hz]')
    axs[1, 0].set_xlabel("frequency Hz")
    axs[1, 0].set_xlim([0, 20])
    '''
    element, freq = mne.time_frequency.psd_welch(individual, fmin = 0, fmax = 20, n_fft = 250)
    axs[1, 1].plot(freq, element[1])
    axs[1, 1].set_title("MNE Welch_PSD", pad= 20)
    axs[1, 1].set_ylabel('Power spectrum ($\mu V^2$)')
    axs[1, 1].set_xlabel("frequency Hz")
    axs[1, 1].set_xlim([0, 20])
    '''
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.subplots_adjust()
    return fig