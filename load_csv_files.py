
import pandas as pd
import os

# Getting file path 
# Use path to load files

def path_file_PCF(path_PCF):
    '''
    Input:  add a path to the main folder 
    Function:   create a list with the path for each csv file 
                in the given directory. Sort the files and 
                divide into runs
    Output: four lists of paths (each list is one run per subject) 
    '''
    # Get the list of all files related to the datasets in a csv format
    # PCF = Psychological and Cognitive Factors in Motor Imagery Brain Computer Interfaces dataset
    PCF_files = os.listdir(path_PCF)
    PCF_files.sort()
    
    # append file name to folder path
    path_PCF_file = []
    for file in PCF_files:
        path = path_PCF + "\\" + file
        path_PCF_file.append(path)
    
    # sepparate files according to runs
    PCF_files_1 = path_PCF_file[:55]
    PCF_files_2 = path_PCF_file[55::3]
    PCF_files_3 = path_PCF_file[56::3]
    PCF_files_4 = path_PCF_file[57::3]  
  
    return PCF_files_1, PCF_files_2, PCF_files_3, PCF_files_4

def load_csv(path_list): 
    '''
    Input:  nested list with csv file paths
    Function:   create dictionary (key is the file name,
                item is the loaded csv file, as a pandas data frame )
    Output: dictionary(name of file, loaded csv file)
    '''
    raw_dic = {}
    for plist in path_list:
        for path in plist:
            name = os.path.basename(path)
            raw_dic[name[:-4]] = pd.read_csv(path)
    return raw_dic
