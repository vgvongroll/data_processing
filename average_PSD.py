
from __future__ import division
import scipy.fft as fft
import matplotlib.pyplot as plt
import numpy as np
import math
import scipy.signal as signal
import pandas as pd

def average_per_run(df):
    """
    Input:  dataframe, with PSD values from all trials
    Function:   return the values of the average per column
    Output: dictionary, with the 8 different groups
            of infromation and its two np.arrays of values 
            to plot the PSD average for each run 
    """
    columns = ["group_name"]
    columns.extend(list(range(376)))
    df_avg = pd.DataFrame(columns=columns)
    columns1 = ["group_name"]
    columns1.extend(list(range(314)))
    df_avg1 = pd.DataFrame(columns=columns1)

    # Create all the different subset 
    df_left = df[df["trial_name"].str.contains("_left_", case=False)]
    df_right = df[df["trial_name"].str.contains("_right_", case=False)]

    df_left_C3 = df_left[df_left["channel"] == "C3"]
    df_left_C4 = df_left[df_left["channel"] == "C4"]
    df_right_C3 = df_right[df_right["channel"] == "C3"]
    df_right_C4 = df_right[df_right["channel"] == "C4"]

    df_left_C3_freq = df_left_C3[df_left_C3["type_info"] == "frequencies"]
    df_left_C3_PSD = df_left_C3[df_left_C3["type_info"] == "PSD"]
    df_left_C4_freq = df_left_C4[df_left_C4["type_info"] == "frequencies"]
    df_left_C4_PSD = df_left_C4[df_left_C4["type_info"] == "PSD"]

    df_right_C3_freq = df_right_C3[df_right_C3["type_info"] == "frequencies"] 
    df_right_C3_PSD = df_right_C3[df_right_C3["type_info"] == "PSD"]
    df_right_C4_freq = df_right_C4[df_right_C4["type_info"] == "frequencies"]
    df_right_C4_PSD = df_right_C4[df_right_C4["type_info"] == "PSD"]

    # get the mean for every subset
    df_left_C3_freq = list(df_left_C3_freq.mean(axis=0, numeric_only = True))
    df_left_C3_PSD = list(df_left_C3_PSD.mean(axis=0, numeric_only = True))
    df_left_C4_freq = list(df_left_C4_freq.mean(axis=0, numeric_only = True))
    df_left_C4_PSD = list(df_left_C4_PSD.mean(axis=0, numeric_only = True))

    df_right_C3_freq = list(df_right_C3_freq.mean(axis=0, numeric_only = True))
    df_right_C3_PSD = list(df_right_C3_PSD.mean(axis=0, numeric_only = True))
    df_right_C4_freq = list(df_right_C4_freq.mean(axis=0, numeric_only = True))
    df_right_C4_PSD = list(df_right_C4_PSD.mean(axis=0, numeric_only = True))

    # combine all the means into one dataframe 
    groups = [df_left_C3_freq, df_left_C3_PSD, df_left_C4_freq, df_left_C4_PSD, df_right_C3_freq, df_right_C3_PSD, df_right_C4_freq, df_right_C4_PSD]
    groups_name = ["left_C3_freq", "left_C3_PSD", "left_C4_freq", "left_C4_PSD", "right_C3_freq", "right_C3_PSD", "right_C4_freq", "right_C4_PSD"]
    len_columns = []
    for group in range(len(groups)):
        groups[group].insert(0, groups_name[group])
        len_columns.append(len(groups[group]))
        try:
            df_avg.loc[len(df_avg)] = groups[group]
        except:
            #return len_columns, len(df_avg1), len(columns1)
            df_avg1.loc[len(df_avg1)] = groups[group]
    return df_avg, df_avg1
   
def PSD_periodogram_fft(trials_dict):
    """
    Input:  Get a dictionary with the data from one run
    Function:   calculate PSD values with signal.periodogram
    Output: pd.DataFrame, with each trial as a row
    """
    columns = ["trial_name", "channel", "type_info"]
    columns.extend(list(range(376))) #MI task == 376
    df = pd.DataFrame(columns=columns)

    columns1 = ["trial_name", "channel", "type_info"]
    columns1.extend(list(range(314))) #rest == 314
    df1 = pd.DataFrame(columns=columns1)

    key_right = []
    key_left = []
    MI_task = [key_left, key_right]
    for task in trials_dict.keys():
        if "_right_" in task:
            key_right.append(task)
        elif "_left_" in task:
            key_left.append(task)
    for i in range(len(MI_task)):
        for trial in MI_task[i]:
            a, b = signal.periodogram(trials_dict[trial][0][0], fs = 250)
            C3 = str(trial)
            a = list(a)
            b= list(b[0])
            a.insert(0, "frequencies")
            a.insert(0, "C3")
            a.insert(0, C3)
            b.insert(0, "PSD")
            b.insert(0, "C3")
            b.insert(0, C3)

            c, d = signal.periodogram(trials_dict[trial][1][0], fs = 250)
            C4 = str(trial)
            c = list(c)
            d = list(d[0])
            c.insert(0, "frequencies")
            c.insert(0, "C4")
            c.insert(0, C4)
            d.insert(0, "PSD")
            d.insert(0, "C4")
            d.insert(0, C4)

            try:
                df.loc[len(df)] = a
                df.loc[len(df)] = b
                df.loc[len(df)] = c
                df.loc[len(df)] = d
            except:
                df1.loc[len(df1)] = a
                df1.loc[len(df1)] = b
                df1.loc[len(df1)] = c
                df1.loc[len(df1)] = d
            
    return  df, df1

'''def PSD_periodogram_fft(trials_dict):
    """
    Input:  Get a dictionary with the data from one run
    Function:   calculate PSD values with signal.periodogram
    Output: pd.DataFrame, with each trial as a row
    """
    columns = ["trial_name", "channel", "type_info"]
    columns1 = ["trial_name", "channel", "type_info"]
    columns.extend(list(range(376)))
    columns1.extend(list(range(314)))
    df = pd.DataFrame(columns=columns)
    df1 = pd.DataFrame(columns=columns1)

    key_right = []
    key_left = []
    MI_task = [key_left, key_right]
    for task in trials_dict.keys():
        if "_right_" in task:
            key_right.append(task)
        elif "_left_" in task:
            key_left.append(task)
    for i in range(len(MI_task)):
        for trial in MI_task[i]:
            a, b = signal.periodogram(trials_dict[trial][0][0], fs = 250)
            C3 = str(trial)
            a = list(a)
            b= list(b[0])
            a.insert(0, "frequencies")
            a.insert(0, "C3")
            a.insert(0, C3)
            b.insert(0, "PSD")
            b.insert(0, "C3")
            b.insert(0, C3)

            c, d = signal.periodogram(trials_dict[trial][1][0], fs = 250)
            C4 = str(trial)
            c = list(c)
            d = list(d[0])
            c.insert(0, "frequencies")
            c.insert(0, "C4")
            c.insert(0, C4)
            d.insert(0, "PSD")
            d.insert(0, "C4")
            d.insert(0, C4)

            try:
                df.loc[len(df)] = a
                df.loc[len(df)] = b
                df.loc[len(df)] = c
                df.loc[len(df)] = d
            except:
                try:
                    df1.loc[len(df)] = a
                    df1.loc[len(df)] = b
                    df1.loc[len(df)] = c
                    df1.loc[len(df)] = d
                except:
                    return (len(a), len(b), len(c), len(d))
            return df1

    return  df'''


def PSD_values(trials_dict):
    """
    Input:  Get a dictionary with the data from one run 
    Function:   Create the PSD values with three different
                values functions 
    Output: Dictionary, that has the PSD values for each 
            individual trial
    """
    Periodogram_scipy_fft = {}
    Spectrum_fourier_transform = {}
    Welch_periodogram = {}
    for trial in trials_dict.keys():
        # Periodogram scipy FFT
        a, b = signal.periodogram(trials_dict[trial][0][0], fs = 250)
        C3 = str(trial) + str("C3")
        c, d = signal.periodogram(trials_dict[trial][1][0], fs = 250)
        C4 = str(trial) + str("C4")
        Periodogram_scipy_fft[trial] = [[C3, a, b], [C4, c, d]] 

        # Fourier transform
        FourierCoeff = np.fft.fft(trials_dict[trial][0][0])/trials_dict[trial][0][0].size
        DC = [np.abs(FourierCoeff[0])]
        amp = np.concatenate((DC, 2*np.abs(FourierCoeff[1:])))  
        # compute frequencies vector until half the sampling rate
        Nsamples = int( math.floor(trials_dict[trial][0][0].size/2) )
        hz = np.linspace(0, 250/2., num = Nsamples + 1 )
        C3 = str(trial) + str("C3")
        FourierCoeff1 = np.fft.fft(trials_dict[trial][1][0])/trials_dict[trial][1][0].size
        DC1 = [np.abs(FourierCoeff1[0])]
        amp1 = np.concatenate((DC1, 2*np.abs(FourierCoeff1[1:])))  
        # compute frequencies vector until half the sampling rate
        Nsamples1 = int( math.floor(trials_dict[trial][1][0].size/2) )
        hz1 = np.linspace(0, 250/2., num = Nsamples1 + 1 )
        C4 = str(trial) + str("C4")
        Spectrum_fourier_transform[trial] = [[C3, hz, amp[0][:len(hz)]], [C4, hz1, amp1[0][:len(hz)]]]

        # Welch periodogram 
        segment = int( 250*2 ) 
        myhann = signal.get_window('hann', segment)
        #obtain Power density (amplitude^2/Hz) withouth tappering
        myparams = dict(fs = 250, nperseg = segment, window = np.ones(segment), 
                        noverlap = 0, scaling = 'density', return_onesided=True)
        freq, psd = signal.welch(x = trials_dict[trial][0][0], **myparams)# units uV**2/Hz
        psd = 2*psd # correct for negative frequencies
        C3 = str(trial) + str("C3")
        freq1, psd1 = signal.welch(x = trials_dict[trial][1][0], **myparams)# units uV**2/Hz
        psd1 = 2*psd # correct for negative frequencies
        C4 = str(trial) + str("C4")
        Welch_periodogram[trial] = [[C3, freq, psd[0]], [C4, freq1, psd1[0]]]

   
    return Periodogram_scipy_fft, Spectrum_fourier_transform, Welch_periodogram 