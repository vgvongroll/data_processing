
import scipy.io as spio
import os

# Getting file path 

def path_file_mat(path_m):
    '''
    Input:   path for EMB = EEG_MI_BCI
    Function:   create a list with paths for each matlab file 
                (if they are not in separated folders)
    Output: list with all the paths for matlab files
    '''
    # creating a list of paths from all EEG signal files that are part of the dataset 
    # This function goes for the files in the dataset from the Inria Team Data A

    m_folder = os.listdir(path_m) # list the files that are at that directory
    # print(m_folder)

    m_files = []
    for file in m_folder:
        if ".mat" in file:
            path = path_m + "\\" + file
            m_files.append(path)
    return m_files
    
# pprint(path_file_Inria())
# print(len(path_file_Inria()))

""" This functions were copied from 
https://stackoverflow.com/questions/7008608/scipy-io-loadmat-nested-structures-i-e-dictionaries 
after I spend a lot of time trying to figure ou how to access 
the nested data in the matlab file. 

Need to check how to cite this 
"""

def loadmat(filename):
    '''
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    '''
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)

def _check_keys(dict):
    '''
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    '''
    for key in dict:
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict        

def _todict(matobj):
    '''
    A recursive function which constructs from matobjects nested dictionaries
    '''
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict

# load all the individual files from the dataset at once 

def load_matlab_data(paths):
    # load matfiles and access nested arrays 
    loaded_data = {}
    for file in range(len(paths)):
        name = os.path.basename(paths[file])
        loaded_data[name[:-4]] = loadmat(paths[file])
    return loaded_data